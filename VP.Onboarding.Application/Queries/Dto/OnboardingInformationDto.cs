﻿using System;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class OnboardingInformationDto
    {
        public string FirstContactPersonFirstName { get; private set; }
        public string FirstContactPersonMiddleName { get; private set; }
        public string FirstContactPersonLastName { get; private set; }
        public string FirstContactPersonEmail { get; private set; }
        public int? DurationOfOnboardingMonths { get; private set; }
        public decimal? MonthlyAmount { get; private set; }
        public DateTime? ContractUntil { get; private set; }

        public OnboardingInformationDto(string firstContactPersonFirstName, string firstContactPersonMiddleName, string firstContactPersonLastName, string firstContactPersonEmail,
                                           int? durationOfOnboardingMonths, decimal? monthlyAmount, DateTime? contractUntil)
        {
            FirstContactPersonFirstName = firstContactPersonFirstName;
            FirstContactPersonMiddleName = firstContactPersonMiddleName;
            FirstContactPersonLastName = firstContactPersonLastName;
            FirstContactPersonEmail = firstContactPersonEmail;
            DurationOfOnboardingMonths = durationOfOnboardingMonths;
            MonthlyAmount = monthlyAmount;
            ContractUntil = contractUntil;
        }

        public OnboardingInformationViewModel ToViewModel()
        {
            return new OnboardingInformationViewModel
            {
                ContractUntil = ContractUntil,
                DurationOfOnboardingMonths = DurationOfOnboardingMonths,
                FirstContactPersonEmail = FirstContactPersonEmail,
                FirstContactPersonFirstName = FirstContactPersonFirstName,
                FirstContactPersonMiddleName = FirstContactPersonMiddleName,
                FirstContactPersonLastName = FirstContactPersonLastName,
                MonthlyAmount = MonthlyAmount
            };
        }
    }
}
