﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class FirmKeyDetailsDto
    {

        public string KvkNumber { get; private set; }
        public string CompanyName { get; private set; }
        public string StandardCompanyClassification { get; private set; }
        public int NoOfEmployees { get; private set; }
        public int NoOfAdministrations { get; private set; }

        public FirmKeyDetailsDto(string kvkNumber, string companyName, string standardCompanyClassification, int noOfEmployees, int noOfAdministrations)
        {
            KvkNumber = kvkNumber;
            CompanyName = companyName;
            StandardCompanyClassification = standardCompanyClassification;
            NoOfEmployees = noOfEmployees;
            NoOfAdministrations = noOfAdministrations;
        }
        public FirmKeyDetailsViewModel ToViewModel()
        {
            return new FirmKeyDetailsViewModel(KvkNumber
                , CompanyName
                , StandardCompanyClassification
                , NoOfEmployees
                , NoOfAdministrations);
        }
    }
}
