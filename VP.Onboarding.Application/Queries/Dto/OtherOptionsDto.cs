﻿using System;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class OtherOptionsDto
    {
        public decimal? DiscountValue { get; private set; }
        public int? DiscountType { get; private set; }
        public DateTime? DiscountUntil { get; private set; }
        public decimal? Credits { get; private set; }
        public DateTime? CreditsUntil { get; private set; }

        public OtherOptionsDto(int? discountType, decimal? discountValue, DateTime? discountUntil,
                                   decimal? credits, DateTime? creditsUntil)
        {
            DiscountType = discountType;
            DiscountValue = discountValue;
            DiscountUntil = discountUntil;
            Credits = credits;
            CreditsUntil = creditsUntil;
        }

        public OtherOptionsViewModel ToViewModel()
        {
            return new OtherOptionsViewModel
            {
                Credits = Credits,
                CreditsUntil = CreditsUntil,
                DiscountValue = DiscountValue,
                DiscountType = DiscountType,
                DiscountUntil = DiscountUntil
            };
        }
    }
}
