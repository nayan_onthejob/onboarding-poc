﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class FirmAddressDto
    {
        public int CountryId { get; private set; }
        public string PostalCode { get; private set; }
        public string HouseNumber { get; private set; }
        public string StreetName { get; private set; }
        public string ExtraAddressLine { get; private set; }
        public string City { get; private set; }
        public string Email { get; private set; }
        public string PhoneNumber { get; private set; }

        public FirmAddressDto() { }
        public FirmAddressDto(int countryId, string postalCode, string houseNumber, string streetName, string extraAddressLine, string city, string email, string phoneNumber)
        {
            CountryId = countryId;
            PostalCode = postalCode;
            HouseNumber = houseNumber;
            StreetName = streetName;
            ExtraAddressLine = extraAddressLine;
            City = city;
            Email = email;
            PhoneNumber = phoneNumber;
        }

        public FirmAddressViewModel ToViewModel()
        {
            return new FirmAddressViewModel(CountryId
                , PostalCode
                , HouseNumber
                , StreetName
                , ExtraAddressLine
                , City
                , Email
                , PhoneNumber);
        }
    }
}
