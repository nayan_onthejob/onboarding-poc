﻿using System.Collections.Generic;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class GeneralInformationDto
    {
        public string FirmName { get; private set; }
        public int TypeOfOffice { get; private set; }
        public int HubspotId { get; private set; }
        public IReadOnlyList<int> Services { get; private set; }
        public string InfineLicenceNumber { get; private set; }

        public GeneralInformationDto(string firmName, int typeOfOffice, int hubspotId,
                                         IReadOnlyList<int> services, string infineLicenceNumber)
        {
            FirmName = firmName;
            TypeOfOffice = typeOfOffice;
            HubspotId = hubspotId;
            Services = services;
            InfineLicenceNumber = infineLicenceNumber;
        }

        public GeneralInformationViewModel ToViewModel()
        {
            return new GeneralInformationViewModel
            {
                FirmName = FirmName,
                HubspotId = HubspotId,
                InfineLicenceNumber = InfineLicenceNumber,
                Services = Services,
                TypeOfOffice = TypeOfOffice
            };
        }
    }
}
