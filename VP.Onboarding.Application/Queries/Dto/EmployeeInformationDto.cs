﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class EmployeeInformationDto
    {
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public bool HasAdminRights { get; private set; }
        private readonly List<int> _selectedTrainings;
        public IReadOnlyList<int> SelectedTrainings => _selectedTrainings;


        public EmployeeInformationDto(string email, string firstName, string middleName, string lastName, bool hasAdminRights, List<int> selectedTrainings)
        {
            Email = email;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            HasAdminRights = hasAdminRights;
            _selectedTrainings = selectedTrainings;
        }

        public EmployeeInformationViewModel ToViewModel()
        {
            return new EmployeeInformationViewModel(Email
                , FirstName
                , MiddleName
                , LastName
                , HasAdminRights
                , _selectedTrainings);
        }
    }
}
