﻿using System;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class SalesViewOfPartnerDto
    {
        public Guid PartnerId { get; set; }
        public int CurrentStep { get; set; }
        public GeneralInformationDto GeneralInformation { get; set; }
        public OnboardingInformationDto OnboardingInformation { get; set; }
        public OtherOptionsDto OtherOptions { get; set; }

        public SalesViewOfPartnerViewModel ToViewModel()
        {
            return new SalesViewOfPartnerViewModel
            {
                PartnerId = PartnerId,
                CurrentStep = CurrentStep,
                GeneralInformation = GeneralInformation?.ToViewModel(),
                OnboardingInformation = OnboardingInformation?.ToViewModel(),
                OtherOptions = OtherOptions?.ToViewModel()
            };
        }

    }
}
