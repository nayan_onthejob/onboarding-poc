﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class OtherSoftwaresUsedDto
    {
        public int? AdministrationSoftwareId { get; private set; }
        public int? PayrollSoftwareId { get; private set; }
        public int? TaxationSoftwareId { get; private set; }

        public OtherSoftwaresUsedDto(int? administrationSoftwareId, int? payrollSoftwareId, int? taxationSoftwareId)
        {
            AdministrationSoftwareId = administrationSoftwareId;
            PayrollSoftwareId = payrollSoftwareId;
            TaxationSoftwareId = taxationSoftwareId;
        }

        public OtherSoftwaresUsedViewModel ToViewModel()
        {
            return new OtherSoftwaresUsedViewModel(AdministrationSoftwareId
                , PayrollSoftwareId
                , TaxationSoftwareId);
        }
    }
}
