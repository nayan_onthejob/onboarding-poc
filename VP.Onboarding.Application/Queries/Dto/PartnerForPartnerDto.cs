﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Dto
{
    public class PartnerForPartnerDto
    {
        public Guid PartnerId { get; set; }
        public int CurrentStep { get; set; }
        public FirmKeyDetailsDto FirmKeyDetailsDto { get; set; }
        public FirmAddressDto FirmAddressDto { get; set; }
        public IReadOnlyList<EmployeeInformationDto> EmployeeInformationDto { get; set; }
        public OtherSoftwaresUsedDto OtherSoftwaresUsedDto { get; set; }

        public PartnerViewOfPartnerViewModel ToViewModel()
        {
            return new PartnerViewOfPartnerViewModel(PartnerId
                , CurrentStep
                , FirmKeyDetailsDto.ToViewModel()
                , FirmAddressDto.ToViewModel()
                , EmployeeInformationDto?.Select(x => x.ToViewModel()).ToList()
                , OtherSoftwaresUsedDto.ToViewModel());
        }
    }
}
