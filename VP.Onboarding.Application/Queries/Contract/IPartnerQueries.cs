﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VP.Onboarding.Application.Queries.ViewModel;

namespace VP.Onboarding.Application.Queries.Contract
{
    public interface IPartnerQueries
    {
        Task<SalesViewOfPartnerViewModel> GetSalesViewOfPartnerByIdAsync(Guid id);
        Task<PartnerViewOfPartnerViewModel> GetPartnerViewOfPartnerByIdAsync(Guid id);

        IReadOnlyList<LookupViewModel> GetOfficeTypes();
        IReadOnlyList<LookupViewModel> GetServiceTypes();
        IReadOnlyList<LookupViewModel> GetSalesSteps();
        IReadOnlyList<PartnerTrainingsViewModel> GetPartnerTrainings();
        IReadOnlyList<LookupViewModel> GetCountries();
        IReadOnlyList<LookupViewModel> GetAdministrationSoftwares();
        IReadOnlyList<LookupViewModel> GetPayrollSoftwares();
        IReadOnlyList<LookupViewModel> GetTaxationSoftwares();
        IReadOnlyList<LookupViewModel> GetDiscountTypes();
        IReadOnlyList<LookupViewModel> GetCountryTypes();
    }
}
