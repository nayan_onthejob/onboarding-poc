﻿using System;
using System.Threading.Tasks;
using VP.Onboarding.Application.Queries.Dto;

namespace VP.Onboarding.Application.Queries.Repositories
{
    public interface IPartnerQueryRepository : IQueryRepository
    {
        Task<SalesViewOfPartnerDto> GetSalesViewOfPartnerByIdAsync(Guid id);
        Task<PartnerForPartnerDto> GetPartnerViewOfPartnerByIdAsync(Guid id);

    }
}
