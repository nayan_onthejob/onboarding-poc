﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VP.Onboarding.Application.Queries.Contract;
using VP.Onboarding.Application.Queries.Dto;
using VP.Onboarding.Application.Queries.Repositories;
using VP.Onboarding.Application.Queries.ViewModel;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Application.Queries.Implementation
{
    public class PartnerQueries : IPartnerQueries
    {
        private readonly IPartnerQueryRepository _partnerQueryRepository;
        public PartnerQueries(IPartnerQueryRepository partnerQueryRepository)
        {
            _partnerQueryRepository = partnerQueryRepository;
        }
        public async Task<SalesViewOfPartnerViewModel> GetSalesViewOfPartnerByIdAsync(Guid id)
        {
            var partnerDto = await _partnerQueryRepository.GetSalesViewOfPartnerByIdAsync(id);

            return partnerDto?.ToViewModel();
        }
        public async Task<PartnerViewOfPartnerViewModel> GetPartnerViewOfPartnerByIdAsync(Guid id)
        {
            var partnerDto = await _partnerQueryRepository.GetPartnerViewOfPartnerByIdAsync(id);

            return partnerDto?.ToViewModel();
        }
        public IReadOnlyList<LookupViewModel> GetOfficeTypes()
        {
            return Enumeration.GetAll<TypeOfOffice>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }

        public IReadOnlyList<LookupViewModel> GetServiceTypes()
        {
            return Enumeration.GetAll<TypeOfService>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetSalesSteps()
        {
            return Enumeration.GetAll<OnboardingStep>().Where(x => x.Id < OnboardingStep.FirmKeyDetails.Id).Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<PartnerTrainingsViewModel> GetPartnerTrainings()
        {
            var basic = Training.GetBasicTrainings().Select(x => new PartnerTrainingsViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName,
                IsBasic = true
            });
            var advanced = Training.GetAdvanceTrainings().Select(x => new PartnerTrainingsViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName,
                IsBasic = false
            });
            return basic.Concat(advanced).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetCountries()
        {
            return Enumeration.GetAll<Country>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetAdministrationSoftwares()
        {
            return Enumeration.GetAll<AdministrationSoftware>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetPayrollSoftwares()
        {
            return Enumeration.GetAll<PayrollSoftware>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetTaxationSoftwares()
        {
            return Enumeration.GetAll<TaxationSoftware>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetDiscountTypes()
        {
            return Enumeration.GetAll<DiscountType>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }
        public IReadOnlyList<LookupViewModel> GetCountryTypes()
        {
            return Enumeration.GetAll<CountryType>().Select(x => new LookupViewModel
            {
                Value = x.Id.ToString(),
                Label = x.DisplayName
            }).ToList();
        }

    }
}
