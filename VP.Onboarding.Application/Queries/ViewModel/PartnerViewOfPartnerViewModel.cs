﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class PartnerViewOfPartnerViewModel
    {
        public Guid PartnerId { get; private set; }
        public int CurrentStep { get; private set; }
        public FirmKeyDetailsViewModel FirmKeyDetails { get; private set; }
        public FirmAddressViewModel FirmAddress { get; private set; }
        public IReadOnlyList<EmployeeInformationViewModel> EmployeeInformation { get; private set; }
        public OtherSoftwaresUsedViewModel OtherSoftwaresUsed { get; private set; }

        public PartnerViewOfPartnerViewModel(Guid partnerId, int currentStep, FirmKeyDetailsViewModel firmKeyDetails, FirmAddressViewModel firmAddress,
            IReadOnlyList<EmployeeInformationViewModel> employeeInformation, OtherSoftwaresUsedViewModel otherSoftwaresUsed)
        {
            PartnerId = partnerId;
            CurrentStep = currentStep;
            FirmKeyDetails = firmKeyDetails;
            FirmAddress = firmAddress;
            EmployeeInformation = employeeInformation;
            OtherSoftwaresUsed = otherSoftwaresUsed;
        }
    }
}
