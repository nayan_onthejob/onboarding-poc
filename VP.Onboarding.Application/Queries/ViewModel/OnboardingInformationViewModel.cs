﻿using System;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class OnboardingInformationViewModel
    {
        public string FirstContactPersonFirstName { get; set; }
        public string FirstContactPersonMiddleName { get; set; }
        public string FirstContactPersonLastName { get; set; }
        public string FirstContactPersonEmail { get; set; }
        public int? DurationOfOnboardingMonths { get; set; }
        public decimal? MonthlyAmount { get; set; }
        public DateTime? ContractUntil { get; set; }
    }
}
