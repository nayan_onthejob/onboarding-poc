﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class FirmAddressViewModel
    {
        public int CountryId { get; private set; }
        public int CountryTypeId { get; private set; }
        public string PostalCode { get; private set; }
        public string HouseNumber { get; private set; }
        public string StreetName { get; private set; }
        public string ExtraAddressLine { get; private set; }
        public string City { get; private set; }
        public string Email { get; private set; }
        public string PhoneNumber { get; private set; }

        public FirmAddressViewModel(int countryId, string postalCode, string houseNumber, string streetName, string extraAddressLine, string city, string email, string phoneNumber)
        {
            CountryId = countryId;
            CountryTypeId = countryId == Country.Netherlands.Id || countryId == default ? CountryType.TheNetherlands.Id : CountryType.Others.Id;
            PostalCode = postalCode;
            HouseNumber = houseNumber;
            StreetName = streetName;
            ExtraAddressLine = extraAddressLine;
            City = city;
            Email = email;
            PhoneNumber = phoneNumber;
        }
    }
}
