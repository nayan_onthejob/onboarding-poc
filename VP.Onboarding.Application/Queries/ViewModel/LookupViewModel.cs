﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class LookupViewModel
    {
        public string Value { get; set; }
        public string Label { get; set; }
    }
}
