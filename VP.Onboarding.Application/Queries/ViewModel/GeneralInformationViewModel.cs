﻿using System.Collections.Generic;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class GeneralInformationViewModel
    {
        public string FirmName { get; set; }
        public int TypeOfOffice { get; set; }
        public int HubspotId { get; set; }
        public IReadOnlyList<int> Services { get; set; }
        public string InfineLicenceNumber { get; set; }
    }
}
