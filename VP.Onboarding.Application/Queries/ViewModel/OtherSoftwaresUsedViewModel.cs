﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class OtherSoftwaresUsedViewModel
    {
        public int? AdministrationSoftwareId { get; private set; }
        public int? PayrollSoftwareId { get; private set; }
        public int? TaxationSoftwareId { get; private set; }

        public OtherSoftwaresUsedViewModel(int? administrationSoftwareId, int? payrollSoftwareId, int? taxationSoftwareId)
        {
            AdministrationSoftwareId = administrationSoftwareId;
            PayrollSoftwareId = payrollSoftwareId;
            TaxationSoftwareId = taxationSoftwareId;
        }
    }
}
