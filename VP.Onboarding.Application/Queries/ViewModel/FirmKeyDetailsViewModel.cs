﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class FirmKeyDetailsViewModel
    {

        public string KvkNumber { get; private set; }
        public string CompanyName { get; private set; }
        public string StandardCompanyClassification { get; private set; }
        public int NoOfEmployees { get; private set; }
        public int NoOfAdministrations { get; private set; }

        public FirmKeyDetailsViewModel(string kvkNumber, string companyName, string standardCompanyClassification, int noOfEmployees, int noOfAdministrations)
        {
            KvkNumber = kvkNumber;
            CompanyName = companyName;
            StandardCompanyClassification = standardCompanyClassification;
            NoOfEmployees = noOfEmployees;
            NoOfAdministrations = noOfAdministrations;
        }
    }
}
