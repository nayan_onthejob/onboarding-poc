﻿using System;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class SalesViewOfPartnerViewModel
    {
        public Guid PartnerId { get; set; }
        public int CurrentStep { get; set; }
        public GeneralInformationViewModel GeneralInformation { get; set; }
        public OnboardingInformationViewModel OnboardingInformation { get; set; }
        public OtherOptionsViewModel OtherOptions { get; set; }
    }
}
