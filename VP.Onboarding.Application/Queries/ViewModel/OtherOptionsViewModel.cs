﻿using System;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class OtherOptionsViewModel
    {
        public decimal? DiscountValue { get; set; }
        public int? DiscountType { get; set; }
        public DateTime? DiscountUntil { get; set; }
        public decimal? Credits { get; set; }
        public DateTime? CreditsUntil { get; set; }
    }
}
