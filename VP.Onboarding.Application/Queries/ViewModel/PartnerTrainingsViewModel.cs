﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;

namespace VP.Onboarding.Application.Queries.ViewModel
{
    public class PartnerTrainingsViewModel
    {
        public string Value { get; set; }
        public string Label { get; set; }
        public bool IsBasic { get; set; }
    }
}
