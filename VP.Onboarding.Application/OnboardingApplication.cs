﻿using System.Reflection;

namespace VP.Onboarding.Application
{
    public static class OnboardingApplication
    {
        public static Assembly Assembly => typeof(OnboardingApplication).Assembly;
    }
}
