﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class UpdateFirmKeyDetailsCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public string KvkNumber { get; private set; }
        public string CompanyName { get; private set; }
        public string StandardCompanyClassification { get; private set; }
        public int NoOfEmployees { get; private set; }
        public int NoOfAdministrations { get; private set; }

        public UpdateFirmKeyDetailsCommand(string kvkNumber, string companyName, string standardCompanyClassification, int noOfEmployees, int noOfAdministrations)
        {
            KvkNumber = kvkNumber;
            CompanyName = companyName;
            StandardCompanyClassification = standardCompanyClassification;
            NoOfEmployees = noOfEmployees;
            NoOfAdministrations = noOfAdministrations;
        }
        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
