﻿using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class SalesReviewAndSubmitCommandHandler : CommandHandler<SalesReviewAndSubmitCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public SalesReviewAndSubmitCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }

        public async override Task<bool> HandleCommandAsync(SalesReviewAndSubmitCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerForSalesByPartnerIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.ReviewAndSubmitSalesChanges();

            return await _partnerRepository.SubmitSalesChangesAsync(partner);
        }
    }
}
