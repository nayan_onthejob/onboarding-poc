﻿using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class UpdateOtherOptionsCommandHandler : CommandHandler<UpdateOtherOptionsCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public UpdateOtherOptionsCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        public async override Task<bool> HandleCommandAsync(UpdateOtherOptionsCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerForSalesByPartnerIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.UpdateOtherOptions(new Discount(request.DiscountValue, Enumeration.FromValue<DiscountType>(request.DiscountType), request.DiscountUntil)
                                  , new Credits(new Amount(request.Credits), request.CreditsUntil));

            return await _partnerRepository.UpdateSalesChangesAsync(partner);
        }
    }
}
