﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class AddEmployeeCommandHandler : CommandHandler<AddEmployeeCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public AddEmployeeCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        public async override Task<bool> HandleCommandAsync(AddEmployeeCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerByIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.AddEmployees(new Employee(new PersonName(request.FirstName, request.MiddleName, request.LastName)
                , new Email(request.Email)
                , request.HasAdminRights
                , request.SelectedTrainings.Select(t => Enumeration.FromValue<Training>(t))
             ));

            return await _partnerRepository.UpdatePartnerChangesAsync(partner);
        }
    }
}
