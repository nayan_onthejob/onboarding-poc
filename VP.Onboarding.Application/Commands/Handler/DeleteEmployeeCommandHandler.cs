﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class DeleteEmployeeCommandHandler : CommandHandler<DeleteEmployeeCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public DeleteEmployeeCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }

        public async override Task<bool> HandleCommandAsync(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerByIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.DeleteEmployees(request.EmployeeEmail);

            return await _partnerRepository.UpdatePartnerChangesAsync(partner);
        }
    }
}
