﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.Shared;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class UpdateOnboardingInformationCommandHandler : CommandHandler<UpdateOnboardingInformationCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public UpdateOnboardingInformationCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        public async override Task<bool> HandleCommandAsync(UpdateOnboardingInformationCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerForSalesByPartnerIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            request.FirstContactPersonEmail = "suzy@visionplanner.com";

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(request.FirstContactPersonFirstName
                                                                        , request.FirstContactPersonMiddleName
                                                                        , request.FirstContactPersonLastName), new Email(request.FirstContactPersonEmail))
                                            , request.DurationOfOnboardingMonths
                                            , new MonthlyAmount(new Amount(request.MonthlyAmount), request.ContractUntil));

            return await _partnerRepository.UpdateSalesChangesAsync(partner);
        }
    }
}
