﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class UpdateFirmAddressCommandHandler : CommandHandler<UpdateFirmAddressCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public UpdateFirmAddressCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        public async override Task<bool> HandleCommandAsync(UpdateFirmAddressCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerByIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.UpdateFirmAddress(new Address(
                request.CountryTypeId == CountryType.TheNetherlands.Id ? Country.Netherlands : Enumeration.FromValue<Country>(request.CountryId)
                , request.PostalCode
                , request.HouseNumber
                , request.StreetName
                , request.ExtraAddressLine
                , request.City
                , new Email(request.Email)
                , new PhoneNumber(request.PhoneNumber)));

            return await _partnerRepository.UpdatePartnerChangesAsync(partner);
        }
    }
}
