﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class UpdateOtherSoftwaresUserCommandHandler : CommandHandler<UpdateOtherSoftwaresUserCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public UpdateOtherSoftwaresUserCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        public async override Task<bool> HandleCommandAsync(UpdateOtherSoftwaresUserCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerByIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.UpdateSoftwaresUsed(Enumeration.FromValue<AdministrationSoftware>(request.AdministrationSoftwareId)
                , Enumeration.FromValue<PayrollSoftware>(request.PayrollSoftwareId)
                , Enumeration.FromValue<TaxationSoftware>(request.TaxationSoftwareId));

            return await _partnerRepository.UpdatePartnerChangesAsync(partner);
        }
    }
}
