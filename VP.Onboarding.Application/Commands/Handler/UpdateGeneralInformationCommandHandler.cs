﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class UpdateGeneralInformationCommandHandler : CommandHandler<UpdateGeneralInformationCommand, bool>
    {
        private readonly IPartnerRepository _partnerRepository;

        public UpdateGeneralInformationCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }
        public async override Task<bool> HandleCommandAsync(UpdateGeneralInformationCommand request, CancellationToken cancellationToken)
        {
            var partner = await _partnerRepository.GetPartnerForSalesByPartnerIdAsync(request.PartnerId);

            if (partner == null)
                throw new ResourceNotFoundException("partner not found");

            partner.UpdateGeneralInformation(request.FirmName
                                        , Enumeration.FromValue<TypeOfOffice>(request.TypeOfOffice)
                                        , request.HubspotId
                                        , request.InfineLicenceNumber
                                        , request.Services.Select(x => Enumeration.FromValue<TypeOfService>(x)));

            return await _partnerRepository.UpdateSalesChangesAsync(partner);
        }
    }
}
