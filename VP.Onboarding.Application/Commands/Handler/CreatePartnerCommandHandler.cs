﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.Mediator.Abstraction;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Application.Commands.Handler
{
    public class CreatePartnerCommandHandler : CommandHandler<CreatePartnerCommand, Guid>
    {
        private readonly IPartnerRepository _partnerRepository;

        public CreatePartnerCommandHandler(IPartnerRepository partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }

        public async override Task<Guid> HandleCommandAsync(CreatePartnerCommand request, CancellationToken cancellationToken)
        {
            var partner = new Partner(request.FirmName);

            partner.UpdateGeneralInformation(request.FirmName
                                            , Enumeration.FromValue<TypeOfOffice>(request.TypeOfOffice)
                                            , request.HubspotId
                                            , request.InfineLicenceNumber
                                            , request.Services.Select(x => Enumeration.FromValue<TypeOfService>(x)));

            await _partnerRepository.CreatePartnerAsync(partner);
            return partner.Id;
        }

    }
}
