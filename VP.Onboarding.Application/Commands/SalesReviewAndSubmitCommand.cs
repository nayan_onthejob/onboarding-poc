﻿using System;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class SalesReviewAndSubmitCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public SalesReviewAndSubmitCommand(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
