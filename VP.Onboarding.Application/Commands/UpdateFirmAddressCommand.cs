﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class UpdateFirmAddressCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public int CountryTypeId { get; set; }
        public int CountryId { get; private set; }
        public string PostalCode { get; private set; }
        public string HouseNumber { get; private set; }
        public string StreetName { get; private set; }
        public string ExtraAddressLine { get; private set; }
        public string City { get; private set; }
        public string Email { get; private set; }
        public string PhoneNumber { get; private set; }

        public UpdateFirmAddressCommand(int? countryId, int countryTypeId, string postalCode, string houseNumber, string streetName, string extraAddressLine, string city, string email, string phoneNumber)
        {
            CountryId = countryId ?? (default);
            CountryTypeId = countryTypeId;
            PostalCode = postalCode;
            HouseNumber = houseNumber;
            StreetName = streetName;
            ExtraAddressLine = extraAddressLine;
            City = city;
            Email = email;
            PhoneNumber = phoneNumber;
        }
        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
