﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class DeleteEmployeeCommand : ICommand<bool>
    {
        public Guid PartnerId { get; set; }
        public string EmployeeEmail { get; set; }

        public DeleteEmployeeCommand(Guid partnerId, string employeeEmail)
        {
            PartnerId = partnerId;
            EmployeeEmail = employeeEmail;
        }
    }
}
