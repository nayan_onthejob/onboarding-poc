﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class AddEmployeeCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public bool HasAdminRights { get; private set; }
        private readonly List<int> _selectedTrainings;
        public IReadOnlyList<int> SelectedTrainings => _selectedTrainings;


        public AddEmployeeCommand(string email, string firstName, string middleName, string lastName, bool hasAdminRights, List<int> selectedTrainings)
        {
            Email = email;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            HasAdminRights = hasAdminRights;
            _selectedTrainings = selectedTrainings;
        }
        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
