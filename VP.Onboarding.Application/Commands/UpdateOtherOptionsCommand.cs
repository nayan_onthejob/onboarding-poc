﻿using System;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class UpdateOtherOptionsCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public int DiscountType { get; private set; }
        public decimal DiscountValue { get; private set; }
        public DateTime DiscountUntil { get; private set; }
        public decimal Credits { get; private set; }
        public DateTime CreditsUntil { get; private set; }

        public UpdateOtherOptionsCommand(decimal minInvoiceAmount, int discountType, decimal discountValue, DateTime discountUntil,
                                   decimal credits, DateTime creditsUntil)
        {
            DiscountType = discountType;
            DiscountValue = discountValue;
            DiscountUntil = discountUntil;
            Credits = credits;
            CreditsUntil = creditsUntil;
        }
        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
