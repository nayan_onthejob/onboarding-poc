﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class UpdateOtherSoftwaresUserCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public int AdministrationSoftwareId { get; private set; }
        public int PayrollSoftwareId { get; private set; }
        public int TaxationSoftwareId { get; private set; }

        public UpdateOtherSoftwaresUserCommand(int administrationSoftwareId, int payrollSoftwareId, int taxationSoftwareId)
        {
            AdministrationSoftwareId = administrationSoftwareId;
            PayrollSoftwareId = payrollSoftwareId;
            TaxationSoftwareId = taxationSoftwareId;
        }
        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
