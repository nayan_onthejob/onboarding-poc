﻿using System;
using System.Collections.Generic;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class UpdateGeneralInformationCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public string FirmName { get; private set; }
        public int TypeOfOffice { get; private set; }
        public int HubspotId { get; private set; }
        public IReadOnlyList<int> Services { get; private set; }
        public string InfineLicenceNumber { get; private set; }

        public UpdateGeneralInformationCommand(string firmName, int typeOfOffice, int hubspotId,
                                         IReadOnlyList<int> services, string infineLicenceNumber)
        {
            FirmName = firmName;
            TypeOfOffice = typeOfOffice;
            HubspotId = hubspotId;
            Services = services;
            InfineLicenceNumber = infineLicenceNumber;
        }

        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
