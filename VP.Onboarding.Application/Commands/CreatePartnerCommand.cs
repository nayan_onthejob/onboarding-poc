﻿using System;
using System.Collections.Generic;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class CreatePartnerCommand : ICommand<Guid>
    {
        public string FirmName { get; private set; }
        public int TypeOfOffice { get; private set; }
        public int HubspotId { get; private set; }
        public IReadOnlyList<int> Services { get; private set; }
        public string InfineLicenceNumber { get; private set; }


        public CreatePartnerCommand(string firmName, int typeOfOffice, int hubspotId,
                                         IReadOnlyList<int> services, string infineLicenceNumber)
        {
            FirmName = firmName;
            TypeOfOffice = typeOfOffice;
            HubspotId = hubspotId;
            Services = services;
            InfineLicenceNumber = infineLicenceNumber;
        }

    }
}
