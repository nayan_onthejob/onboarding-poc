﻿using System;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Application.Commands
{
    public class UpdateOnboardingInformationCommand : ICommand<bool>
    {
        public Guid PartnerId { get; private set; }
        public string FirstContactPersonFirstName { get; private set; }
        public string FirstContactPersonMiddleName { get; private set; }
        public string FirstContactPersonLastName { get; private set; }
        public string FirstContactPersonEmail { get; set; }
        public int DurationOfOnboardingMonths { get; private set; }
        public decimal MonthlyAmount { get; private set; }
        public DateTime ContractUntil { get; private set; }

        public UpdateOnboardingInformationCommand(string firstContactPersonFirstName, string firstContactPersonMiddleName, string firstContactPersonLastName, string firstContactPersonEmail,
                                            int durationOfOnboardingMonths, decimal monthlyAmount, DateTime contractUntil)
        {
            FirstContactPersonFirstName = firstContactPersonFirstName;
            FirstContactPersonMiddleName = firstContactPersonMiddleName;
            FirstContactPersonLastName = firstContactPersonLastName;
            FirstContactPersonEmail = firstContactPersonEmail;
            DurationOfOnboardingMonths = durationOfOnboardingMonths;
            MonthlyAmount = monthlyAmount;
            ContractUntil = contractUntil;
        }

        public void SetPartnerId(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}
