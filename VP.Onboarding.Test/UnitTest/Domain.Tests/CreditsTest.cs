﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using Xunit;

namespace VP.Onboarding.Test.UnitTest.Domain.Tests
{
    [Collection(nameof(CreditsTest))]
    public class CreditsTest
    {
        private const int AmountValue = 12;
        
        [Fact(DisplayName = "create new credits with valid data")]
        public void CreateCredits_WithValidDetails_ShouldCreateCredits()
        {
            var creditsUntil = DateTime.Now;
            Credits credits = new Credits(new Amount(AmountValue), creditsUntil);

            credits.Amount.Value.Should().Be(12);
            credits.CreditsUntil.Should().Be(creditsUntil);
        }
        [Fact(DisplayName = "create new credits with invalid data should throw exception")]
        public void CreateCredits_WithInValidDetails_ShouldThrowException()
        {
            Action act = () => new Credits(new Amount(12), default(DateTime));

            act.Should().Throw<DomainException>().WithMessage(Constant.CREDITS_UNTILL_DATE_IS_MANDATORY_WHEN_VALUE_IS_PROVIDED);
        }
    }
}
