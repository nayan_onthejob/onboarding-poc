﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using Xunit;

namespace VP.Onboarding.Test.UnitTest.Domain.Tests
{
    [Collection(nameof(PartnerTest))]
    public class PartnerTest
    {
        [Fact(DisplayName = "update general information to partner with valid data should add general information")]
        public void UpdateGeneralInformation_WithValidDetails_ShouldAddAddGeneralInformationToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);

            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.FirmName.Should().Be(TestData.FIRM_NAME);
            partner.TypeOfOffice.Should().Be(TypeOfOffice.Accountancy);
            partner.HubspotId.Should().Be(TestData.HUBSPOT_ID);
            partner.InfineLicenceNumber.Should().Be(TestData.INFINE_LICENCE_NUMBER);

            partner.SelectedServices.Should().HaveCount(1);
        }
        [Fact(DisplayName = "update general information to partner invalid data should raise exception")]
        public void UpdateGeneralInformation_WithInValidDetails_ShouldThrowException()
        {

            AssertDomainExceptionsOfUpdateGeneralInformation(Constant.FIRM_NAME_IS_MANDATORY, string.Empty, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID,
                TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            AssertDomainExceptionsOfUpdateGeneralInformation(Constant.TYPE_OF_OFFICE_IS_MANDATORY, TestData.FIRM_NAME, null, TestData.HUBSPOT_ID,
               TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            AssertDomainExceptionsOfUpdateGeneralInformation(Constant.INFINE_LICENCE_NUMBER_IS_MANDATORY, TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID,
              string.Empty, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            AssertDomainExceptionsOfUpdateGeneralInformation(Constant.HUBSPOT_ID_NUMBER_IS_MANDATORY, TestData.FIRM_NAME, TypeOfOffice.Accountancy, default(int),
              TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            AssertDomainExceptionsOfUpdateGeneralInformation(Constant.ATLEAST_ONE_SERVICE_IS_MANDATORY, TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID,
              TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { });
        }
        [Fact(DisplayName = "update onboarding information to partner with valid data should add onboarding information")]
        public void UpdateOnboardingInformation_WithValidDetails_ShouldAddOnboardingInformationToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            var contractUntil = DateTime.Now;
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                             TestData.DURATION_OF_ONBARDING,
                                             new MonthlyAmount(new Amount(12), contractUntil));

            partner.FirstContact.PersonName.FirstName.Should().Be(TestData.FIRST_NAME);
            partner.FirstContact.PersonName.MiddleName.Should().Be(TestData.MIDDLE_NAME);
            partner.FirstContact.PersonName.LastName.Should().Be(TestData.LAST_NAME);
            partner.FirstContact.Email.Value.Should().Be(TestData.EMAIL);
            partner.DurationOfOnbarding.Should().Be(TestData.DURATION_OF_ONBARDING);
            partner.MonthlyAmount.ContractUntil.Should().Be(contractUntil);
            partner.MonthlyAmount.Amount.Value.Should().Be(12);
            partner.DurationOfOnbarding.Should().Be(TestData.DURATION_OF_ONBARDING);
        }
        [Fact(DisplayName = "update onboarding information to partner with only mandatory details data should add onboarding information")]
        public void UpdateOnboardingInformation_WithOnlyMandatoryDetails_ShouThrowAddOnboardingInformationToPartner()
        {

            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)), default(int), null);


            partner.FirstContact.PersonName.FirstName.Should().Be(TestData.FIRST_NAME);
            partner.FirstContact.PersonName.MiddleName.Should().Be(TestData.MIDDLE_NAME);
            partner.FirstContact.PersonName.LastName.Should().Be(TestData.LAST_NAME);
            partner.DurationOfOnbarding.Should().Be(0);
        }
        [Fact(DisplayName = "update onboarding information to partner without general information should throw exception")]
        public void UpdateOnboardingInformation_WithOutGeneralInformation_ShouThrowException()
        {

            Partner partner = new Partner(TestData.FIRM_NAME);
           
            Action act = () => partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)), default(int), null);
            act.Should().Throw<DomainException>().WithMessage(Constant.GENERAL_INFORMATION_IS_MANDATORY_TO_ADD_ONBOARDING_INFORMATION);

        }
        [Fact(DisplayName = "update onboarding information to partner invalid data should raise exception")]
        public void UpdateOnboardingInformation_WithInValidDetails_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            Action act = () => partner.UpdateOnboardingInformation(null
                                                            , TestData.DURATION_OF_ONBARDING
                                                            , new MonthlyAmount(new Amount(12)
                                                            , DateTime.Now));
            act.Should().Throw<DomainException>().WithMessage(Constant.FIRST_CONTACT_DETAILS_ARE_MANDATORY);
        }
        [Fact(DisplayName = "update onboarding information to partner when provided mounthly amount without contract until raise exception")]
        public void UpdateOnboardingInformation_WhenProvidedMounthlyAmountWithoutContractUntil_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            Action act = () => partner.UpdateOnboardingInformation(null
                                                            , TestData.DURATION_OF_ONBARDING
                                                            , new MonthlyAmount(new Amount(12)
                                                            , default(DateTime)));
            act.Should().Throw<DomainException>().WithMessage(Constant.CONTRACT_UNTILL_DATE_IS_MANDATORY_WHEN_MONTHLY_AMOUNT_IS_PROVIDED);
        }

        [Fact(DisplayName = "update other options to partner with valid data should add onboarding information")]
        public void UpdateOtherOptions_WithValidDetails_ShouldAddOtherOptionsToPartner()
        {
            var creditsUntil = DateTime.Now.AddDays(-2);
            var discountUntil = DateTime.Now.AddDays(-2);
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                             TestData.DURATION_OF_ONBARDING,
                                             new MonthlyAmount(new Amount(12), DateTime.Now));

            partner.UpdateOtherOptions(new Discount(12, DiscountType.Amount, discountUntil)
                                    , new Credits(new Amount(12), creditsUntil));


            partner.Discount.Value.Should().Be(12);
            partner.Credits.Amount.Value.Should().Be(12);
            partner.Discount.DiscountType.Should().Be(DiscountType.Amount);
            partner.Credits.CreditsUntil.Should().Be(creditsUntil);
            partner.Discount.DiscountUntil.Should().Be(discountUntil);
        }
        [Fact(DisplayName = "update other options to partner with default data should add onboarding information")]
        public void UpdateOtherOptions_WithDefaultDetails_ShouldAddOtherOptionsToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                            TestData.DURATION_OF_ONBARDING,
                                            new MonthlyAmount(new Amount(12), DateTime.Now));

            partner.UpdateOtherOptions(new Discount(default(decimal), DiscountType.Percentage, default(DateTime))
                                    , new Credits(new Amount(default(decimal)), default(DateTime)));


            partner.Discount.Value.Should().Be(default(decimal));
        }
        [Fact(DisplayName = "update other options to partner with discount until greater than contract until should throw exception")]
        public void UpdateOtherOptions_WithDiscountUntilGreaterThanContractUntilDetails_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                           TestData.DURATION_OF_ONBARDING,
                                           new MonthlyAmount(new Amount(12), DateTime.Now));


            Action act = () => partner.UpdateOtherOptions(new Discount(12, DiscountType.Amount, DateTime.Now.AddDays(1))
                                    , new Credits(new Amount(12), DateTime.Now.AddDays(-1)));

            act.Should().Throw<DomainException>().WithMessage(Constant.DISCOUNT_UNTILL_SHOULD_NOT_BE_GREATER_THAN_CONTRACT_UNTILL);
        }
        [Fact(DisplayName = "update other options to partner with contract until greater than contract until should throw exception")]
        public void UpdateOtherOptions_WithContractUntilProvidedAndCreditsProvidedButCreditsUntilGreaterThanContractUntil_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                           TestData.DURATION_OF_ONBARDING,
                                           new MonthlyAmount(new Amount(12), DateTime.Now.AddDays(-1)));


            Action act = () => partner.UpdateOtherOptions(new Discount(12, DiscountType.Amount, DateTime.Now.AddDays(-2))
                                    , new Credits(new Amount(12), DateTime.Now.AddDays(1)));

            act.Should().Throw<DomainException>().WithMessage(Constant.CREDITS_UNTILL_SHOULD_NOT_BE_GREATER_THAN_CONTRACT_UNTILL);
        }

        [Fact(DisplayName = "review and submit without other options should throw exception")]
        public void ReviewAndSubmitSalesChanges_WithoutOtherOptions_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                           TestData.DURATION_OF_ONBARDING,
                                           new MonthlyAmount(new Amount(12), DateTime.Now.AddDays(-1)));


            Action act = () => partner.ReviewAndSubmitSalesChanges();

            act.Should().Throw<DomainException>().WithMessage(Constant.GENERALINFORMATION_ONBOARDING_INFORMATION_AND_OTHER_OPTIONS_ARE_MANDATORY);
        }
        [Fact(DisplayName = "review and submit all information should return true")]
        public void ReviewAndSubmitSalesChanges_WithAllInformation_ShouldReturnTrue()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            partner.UpdateOnboardingInformation(new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL)),
                                           TestData.DURATION_OF_ONBARDING,
                                           new MonthlyAmount(new Amount(12), DateTime.Now));

            partner.UpdateOtherOptions(new Discount(12, DiscountType.Amount, DateTime.Now.AddDays(-1))
                                    , new Credits(new Amount(12), DateTime.Now.AddDays(-1)));

            partner.ReviewAndSubmitSalesChanges();

        }

        [Fact(DisplayName = "update other options to partner without onboarding information should throw exception")]
        public void UpdateOtherOptions_WithoutOtherOptions_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateGeneralInformation(TestData.FIRM_NAME, TypeOfOffice.Accountancy, TestData.HUBSPOT_ID, TestData.INFINE_LICENCE_NUMBER, new List<TypeOfService> { TypeOfService.AdvisoryServices });

            Action act = () => partner.UpdateOtherOptions(new Discount(12, DiscountType.Amount, DateTime.Now.AddDays(1))
                                    , new Credits(new Amount(12), DateTime.Now.AddDays(-1)));

            act.Should().Throw<DomainException>().WithMessage(Constant.ONBOARDING_INFORMATION_IS_MANDATORY_TO_ADD_OTHER_OPTIONS);
        }

        private static void AssertDomainExceptionsOfUpdateGeneralInformation(string ExceptionMessage, string firmName, TypeOfOffice typeOfOffice, int hubspotId, string infineLicenceNumber, IEnumerable<TypeOfService> selectedTypeOfServices)
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            Action act = () => partner.UpdateGeneralInformation(firmName, typeOfOffice, hubspotId, infineLicenceNumber, selectedTypeOfServices);
            act.Should().Throw<DomainException>().WithMessage(ExceptionMessage);
        }

        [Fact(DisplayName = "update firmkey details to partner with valid data should update firm Key Deatils")]
        public void UpdateFirmKeyDetails_WithValidDetails_ShouldUpdateFirmKeyDetailsToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateFirmKeyDetails(TestData.KVK_NUMBER, TestData.COMPANY_NAME, TestData.STANDARD_COMPANY_CLASIFICATION, TestData.NO_OF_EMPLOYEES, TestData.NO_OF_ADMINISTRATIONS);
            partner.KvkNumber.Should().Be(TestData.KVK_NUMBER);
            partner.CompanyName.Should().Be(TestData.COMPANY_NAME);
            partner.StandardCompanyClassification.Should().Be(TestData.STANDARD_COMPANY_CLASIFICATION);
            partner.NoOfEmployees.Should().Be(TestData.NO_OF_EMPLOYEES);
            partner.NoOfAdministrations.Should().Be(TestData.NO_OF_ADMINISTRATIONS);

        }

        [Fact(DisplayName = "update firm key details to partener inavlid data should raise exception")]
        public void UpdateFirmKeyDetails_WithInvalidDetails_ShouldThrowException()
        {
            AssertDomainExceptionsOfUpdateFirmKeyDetails(Constant.KVK_NUMBER_IS_MANDATORY, string.Empty, TestData.COMPANY_NAME, TestData.STANDARD_COMPANY_CLASIFICATION, TestData.NO_OF_EMPLOYEES, TestData.NO_OF_ADMINISTRATIONS);
            AssertDomainExceptionsOfUpdateFirmKeyDetails(Constant.COMPANY_NAME_IS_MANDATORY, TestData.KVK_NUMBER, string.Empty, TestData.STANDARD_COMPANY_CLASIFICATION, TestData.NO_OF_EMPLOYEES, TestData.NO_OF_ADMINISTRATIONS);
            AssertDomainExceptionsOfUpdateFirmKeyDetails(Constant.STANDARD_COMPANY_CLASSIFICATION_IS_MANDATORY, TestData.KVK_NUMBER, TestData.COMPANY_NAME, string.Empty, TestData.NO_OF_EMPLOYEES, TestData.NO_OF_ADMINISTRATIONS);
            AssertDomainExceptionsOfUpdateFirmKeyDetails(Constant.INVALID_NUMBER_OF_EMPLOYEES, TestData.KVK_NUMBER, TestData.COMPANY_NAME, TestData.STANDARD_COMPANY_CLASIFICATION, default(int), TestData.NO_OF_ADMINISTRATIONS);
            AssertDomainExceptionsOfUpdateFirmKeyDetails(Constant.INVALID_NUMBER_NO_OF_ADMINISTRATIONS, TestData.KVK_NUMBER, TestData.COMPANY_NAME, TestData.STANDARD_COMPANY_CLASIFICATION, TestData.NO_OF_EMPLOYEES, default(int));

        }
        private void AssertDomainExceptionsOfUpdateFirmKeyDetails(string exceptionMessage, string kvkNumber, string companyName, string standardCompanyClassification, int noOfEmployees, int noOfAdministrations)
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            Action act = () => partner.UpdateFirmKeyDetails(kvkNumber, companyName, standardCompanyClassification, noOfEmployees, noOfAdministrations);
            act.Should().Throw<DomainException>().WithMessage(exceptionMessage);
        }

        [Fact(DisplayName = "update firm address to partener inavlid data should raise exception")]
        public void UpdateFirmAddress_WithInvalidDetails_ShouldThrowException()
        {
            AssertDomainExceptionsOfUpdateFirmAddress(Constant.COUNTRY_IS_MANDATORY, new Address(null, TestData.POSTAL_CODE, null, null, null, TestData.CITY, new Email(TestData.EMAIL), new PhoneNumber(TestData.PHONE_NUMBER)));
            AssertDomainExceptionsOfUpdateFirmAddress(Constant.POSTAL_CODE_IS_MANDATORY, new Address(Country.Netherlands, null, null, null, null, TestData.CITY, new Email(TestData.EMAIL), new PhoneNumber(TestData.PHONE_NUMBER)));
            AssertDomainExceptionsOfUpdateFirmAddress(Constant.STREET_NAME_IS_MANDATORY, new Address(Country.Netherlands, TestData.POSTAL_CODE, null, null, null, TestData.CITY, new Email(TestData.EMAIL), new PhoneNumber(TestData.PHONE_NUMBER)));
            AssertDomainExceptionsOfUpdateFirmAddress(Constant.CITY_IS_MANDATORY, new Address(Country.Netherlands, TestData.POSTAL_CODE, null, TestData.STREET_NAME, null, null, new Email(TestData.EMAIL), new PhoneNumber(TestData.PHONE_NUMBER)));
            AssertDomainExceptionsOfUpdateFirmAddress(Constant.EMAIL_IS_MANDATORY, new Address(Country.Netherlands, TestData.POSTAL_CODE, null, TestData.STREET_NAME, null, TestData.CITY, new Email(string.Empty), new PhoneNumber(TestData.PHONE_NUMBER)));
            AssertDomainExceptionsOfUpdateFirmAddress(Constant.PHONE_NUMBER_IS_MANDATORY, new Address(Country.Netherlands, TestData.POSTAL_CODE, null, TestData.STREET_NAME, null, TestData.CITY, new Email(TestData.EMAIL), new PhoneNumber(string.Empty)));
        }
        private void AssertDomainExceptionsOfUpdateFirmAddress(string exceptionMessage, Address address)
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            Action act = () => partner.UpdateFirmAddress(address);
            act.Should().Throw<DomainException>().WithMessage(exceptionMessage);
        }

        [Fact(DisplayName = "update firm address to partner with valid data should add general information")]
        public void UpdateFirmAddress_WithValidDetails_ShouldUpdateUpdateFirmAddressToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.UpdateFirmAddress(new Address(Country.Netherlands, TestData.POSTAL_CODE, null, TestData.STREET_NAME, null, TestData.CITY, new Email(TestData.EMAIL), new PhoneNumber(TestData.PHONE_NUMBER)));
            partner.FirmAddress.Should().NotBeNull();
            partner.FirmAddress.ExtraAddressLine.Should().BeNull();
            partner.FirmAddress.HouseNumber.Should().BeNull();
        }

        [Fact(DisplayName = "add employees to partener inavlid data should raise exception")]
        public void AddEmployees_WithInvalidDetails_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            var employee = new Employee(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL), false, new List<Training> { Training.BasicTraining });
            partner.AddEmployees(employee);
            Action act = () => partner.AddEmployees(employee);
            act.Should().Throw<DomainException>().WithMessage(Constant.EMPLOYEE_ALREADY_EXISTS);
        }

        [Fact(DisplayName = "add employees to partner with valid data should add employee information")]
        public void AddEmployees_WithValidDetails_ShouldAddEmployeesToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.AddEmployees(new Employee(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL), false, new List<Training> { Training.BasicTraining }));
            partner.Employees.Should().NotBeNull();
            partner.Employees.First().Name.FirstName.Should().Be(TestData.FIRST_NAME);
            partner.Employees.First().HasAdminRights.Should().Be(false);
            partner.Employees.First().RequiredTrainings.First().Should().Be(Training.BasicTraining);
            partner.Employees.Should().NotBeNull();
        }

        [Fact(DisplayName = "delete employees to partener inavlid data should raise exception")]
        public void DeleteEmployees_WithInvalidDetails_ShouldThrowException()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            Action act = () => partner.DeleteEmployees(TestData.EMAIL);
            act.Should().Throw<DomainException>().WithMessage(Constant.NO_SUCH_EMPLOYEE_EXISTS);
        }

        [Fact(DisplayName = "delete employees to partner with valid data should delete employee information")]
        public void DeleteEmployees_WithValidDetails_ShouldAddEmployeesToPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            partner.AddEmployees(new Employee(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL), false, new List<Training> { Training.BasicTraining }));
            partner.DeleteEmployees(TestData.EMAIL);
            partner.Employees.Should().NotBeNull();
        }

        [Fact(DisplayName = "update softwares used in partner with valid data should add softwares used details")]
        public void UpdateSoftwaresUsed_WithValidDetails_ShouldUpdateSoftwaresUsedInPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);

            partner.UpdateSoftwaresUsed(AdministrationSoftware.AccountView, PayrollSoftware.Loket, TaxationSoftware.GeneralInformation);

            partner.AdministrationSoftware.Should().Be(AdministrationSoftware.AccountView);
            partner.PayrollSoftware.Should().Be(PayrollSoftware.Loket);
            partner.TaxationSoftware.Should().Be(TaxationSoftware.GeneralInformation);
        }
        [Fact(DisplayName = "update softwares used in partner with valid data should add softwares used details")]
        public void UpdateSoftwaresUsed_WithoutValidDetails_ShouldUpdateSoftwaresUsedInPartner()
        {
            Partner partner = new Partner(TestData.FIRM_NAME);
            Action act = () => partner.UpdateSoftwaresUsed(null, PayrollSoftware.Loket, TaxationSoftware.GeneralInformation);

            act.Should().Throw<DomainException>().WithMessage(Constant.ADMINISTRATION_SOFTWARE_USED_IS_MANDATORY);

            
             act = () => partner.UpdateSoftwaresUsed(AdministrationSoftware.AccountView, null, TaxationSoftware.GeneralInformation);

            act.Should().Throw<DomainException>().WithMessage(Constant.PAYROLL_SOFTWARE_USED_IS_MANDATORY);

            act = () => partner.UpdateSoftwaresUsed(AdministrationSoftware.AccountView, PayrollSoftware.Loket, null);

            act.Should().Throw<DomainException>().WithMessage(Constant.TAXATION_SOFTWARE_USED_IS_MANDATORY);

        }
    }
}
