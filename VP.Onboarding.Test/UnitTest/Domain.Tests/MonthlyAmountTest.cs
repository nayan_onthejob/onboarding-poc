﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using Xunit;

namespace VP.Onboarding.Test.UnitTest.Domain.Tests
{
    [Collection(nameof(MonthlyAmountTest))]
    public class MonthlyAmountTest
    {
        [Fact(DisplayName = "create new monthly amount with valid data")]
        public void CreateMonthlyAmount_WithValidDetails_ShouldCreateMonthlyAmount()
        {
            var contractUntil = DateTime.Now;
            var monthlyAmount = new MonthlyAmount(new Amount(12), contractUntil);

            monthlyAmount.Amount.Value.Should().Be(12);
            monthlyAmount.ContractUntil.Should().Be(contractUntil);
        }
        [Fact(DisplayName = "create new monthly amount with invalid data should throw exception")]
        public void CreateMonthlyAmount_WithInValidDetails_ShouldThrowException()
        {
            Action act = () => new MonthlyAmount(new Amount(12), default(DateTime));

            act.Should().Throw<DomainException>().WithMessage("Contract Until date is mandatory when monthly amount is provided");
        }
        [Fact(DisplayName = "create new monthly amount with invalid data should throw exception")]
        public void CreateMonthlyAmount_WithDefaultValidAmountDetails_ShouldCreateMonthlyAmount()
        {
            var monthlyAmount = new MonthlyAmount(new Amount(default(decimal)), default(DateTime));

            monthlyAmount.Amount.Value.Should().Be(default(decimal));
            monthlyAmount.ContractUntil.Should().Be(default(DateTime));
        }

    }
}
