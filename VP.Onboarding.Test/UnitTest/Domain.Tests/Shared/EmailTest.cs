﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using Xunit;

namespace VP.Onboarding.Test.UnitTest.Domain.Tests.Shared
{
    [Collection(nameof(CreditsTest))]
    public class EmailTest
    {
        [Fact(DisplayName = "create new email with valid data")]
        public void CreateEmail_WithValidDetails_ShouldCreateEmail()
        {
            Email email = new Email("test@test.com");

            email.Value.Should().Be("test@test.com");
        }
        [Fact(DisplayName = "create new email with invalid data should throw exception")]
        public void CreateEmail_WithInValidDetails_ShouldThrowException()
        {
            Action act = () => new Email("sdsad@sad");

            act.Should().Throw<DomainException>().WithMessage("Invalid email format");
        }
    }
}
