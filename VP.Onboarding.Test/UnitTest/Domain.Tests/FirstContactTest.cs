﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;
using Xunit;

namespace VP.Onboarding.Test.UnitTest.Domain.Tests
{
    [Collection(nameof(FirstContactTest))]
    public class FirstContactTest
    {
        [Fact(DisplayName = "create new first contact with valid data")]
        public void CreateFirstContact_WithValidDetails_ShouldCreateFirstContact()
        {
            FirstContact firstContact = new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(TestData.EMAIL));

            firstContact.PersonName.FirstName.Should().Be(TestData.FIRST_NAME);
            firstContact.PersonName.MiddleName.Should().Be(TestData.MIDDLE_NAME);
            firstContact.PersonName.LastName.Should().Be(TestData.LAST_NAME);
            firstContact.Email.Value.Should().Be(TestData.EMAIL);
        }
        [Fact(DisplayName = "create new first contact with invalid name should throw exception")]
        public void CreateFirstContact_WithInValidName_ShouldThrowException()
        {
            Action act = () => new FirstContact(null, new Email(TestData.EMAIL));

            act.Should().Throw<DomainException>().WithMessage(Constant.FIRST_CONTACT_NAME_IS_MANDATORY);
        }
        [Fact(DisplayName = "create new first contact with invalid email should throw exception")]
        public void CreateFirstContact_WithInValidEmail_ShouldThrowException()
        {
            Action act = () => new FirstContact(new PersonName(TestData.FIRST_NAME, TestData.MIDDLE_NAME, TestData.LAST_NAME), new Email(string.Empty));

            act.Should().Throw<DomainException>().WithMessage(Constant.EMAIL_IS_MANDATORY);
        }
    }
}
