﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using Xunit;

namespace VP.Onboarding.Test.UnitTest.Domain.Tests
{
    [Collection(nameof(DiscountTests))]
    public class DiscountTests
    {
        [Fact(DisplayName = "create new discount with valid data")]
        public void CreateCredits_WithValidDetails_ShouldCreateCredits()
        {
            var discountUntil = DateTime.Now;
            Discount discount = new Discount(12, DiscountType.Amount, discountUntil);

            discount.Value.Should().Be(12);
            discount.DiscountUntil.Should().Be(discountUntil);
        }
        [Fact(DisplayName = "create new discount with invalid data should throw exception")]
        public void CreateCredits_WithInValidDiscountUntilDetails_ShouldThrowException()
        {
            Action act = () => new Discount(12, DiscountType.Amount, default(DateTime));

            act.Should().Throw<DomainException>().WithMessage(Constant.DISCOUNT_UNTILL_DATE_IS_MANDATORY);
        }
        [Fact(DisplayName = "create new discount with invalid discount should throw exception")]
        public void CreateCredits_WithInValidDiscountDetails_ShouldThrowException()
        {
            Action act = () => new Discount(120, DiscountType.Percentage, DateTime.Now);

            act.Should().Throw<DomainException>().WithMessage(Constant.INVALID_DISCOUNT);
        }
    }
}
