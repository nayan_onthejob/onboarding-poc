﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VP.Onboarding.Test.UnitTest
{
    internal static class TestData
    {
        public const string FIRM_NAME = "TestFirmName";
        public const string INFINE_LICENCE_NUMBER = "infineLicenceNumber";
        public const int HUBSPOT_ID = 1234;
        
        public const string FIRST_NAME = "TestName";
        public const string MIDDLE_NAME = "TestName";
        public const string LAST_NAME = "TestName";
        public const string EMAIL = "test@email.com";
        public const int DURATION_OF_ONBARDING = 12;

        public const int NO_OF_EMPLOYEES = 1;
        public const int NO_OF_ADMINISTRATIONS = 1;
        public const string STANDARD_COMPANY_CLASIFICATION = "Test standard comapny clasification";
        public const string COMPANY_NAME = "Test company";
        public const string KVK_NUMBER = "Test 123";

        

        public const string COUNTRY = "test country";
        public const string POSTAL_CODE = "3890";
        public const string STREET_NAME = "test street";
        public const string CITY = "test city";
        public const string PHONE_NUMBER = "test phone number";

        
    }
}
