﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Linq;
using VP.Onboarding.Api.Filters;
using VP.Onboarding.Api.Swagger;
using VP.Onboarding.Application;
using VP.Onboarding.Common;
using VP.Onboarding.Domain;
using VP.Onboarding.Infrastructure;

namespace VP.Onboarding.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IHostingEnvironment env, IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(OnboardingApiExceptionFilterAttribute));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //TODO: Add Authorization
            // services.AddAuthorization(Configuration);
            
            services.AddResponseCaching();

            services.AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddApiVersioning(options => options.ReportApiVersions = true);
            services.RegisterSwagger();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Scan(scan => scan
                   .FromAssemblies(OnboardingApplication.Assembly, OnboardingCommon.Assembly, OnboardingDomain.Assembly, OnboardingInfrastructure.Assembly)
               .AddClasses()
               .AsImplementedInterfaces()
               .WithTransientLifetime()
           );
            
            services.AddBatavInfrastructure();

            services.AddMediatR(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider, ILoggerFactory loggerFactory)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseResponseCaching();
            app.UseBatavInfrastructure();
            app.UseMvc();
            //app.UseAuthentication();

            app.UseSwagger();
            if (Configuration["isSwaggerEnabled"].ToLower() != "false")
            {
                app.UseSwaggerVisualInterface(provider);
            }
        }
    }
}
