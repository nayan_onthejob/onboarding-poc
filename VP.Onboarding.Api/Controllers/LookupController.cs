﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using VP.Onboarding.Application.Queries.Contract;

namespace VP.Onboarding.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}")]
    [ApiController]
    [ResponseCache(Duration = 600)]
    public class LookupController : ControllerBase
    {

        private readonly IPartnerQueries _partnerQueries;

        public LookupController(IPartnerQueries partnerQueries)
        {
            _partnerQueries = partnerQueries;
        }

        [Route("partner/trainings")]
        [HttpGet]
        public IActionResult GetAllPartnerTrainings()
        {
            var trainings = _partnerQueries.GetPartnerTrainings();

            return trainings != null ? Ok(new { trainings }) : (IActionResult)NoContent();
        }

        [Route("lookup/countries")]
        [HttpGet]
        public IActionResult GetAllCountries()
        {
            var countries = _partnerQueries.GetCountries();

            return countries != null ? Ok(new { countries }) : (IActionResult)NoContent();
        }
        [Route("partner/administration-softwares")]
        [HttpGet]
        public IActionResult GetAllAdministrationSoftwares()
        {
            var administrationSoftwares = _partnerQueries.GetAdministrationSoftwares();

            return administrationSoftwares != null ? Ok(new { administrationSoftwares }) : (IActionResult)NoContent();
        }
        [Route("partner/payroll-softwares")]
        [HttpGet]
        public IActionResult GetAllPayrollSoftwares()
        {
            var payrollSoftwares = _partnerQueries.GetPayrollSoftwares();

            return payrollSoftwares != null ? Ok(new { payrollSoftwares }) : (IActionResult)NoContent();
        }
        [Route("partner/taxation-softwares")]
        [HttpGet]
        public IActionResult GetAllTaxationSoftwares()
        {
            var taxationSoftwares = _partnerQueries.GetTaxationSoftwares();

            return taxationSoftwares != null ? Ok(new { taxationSoftwares }) : (IActionResult)NoContent();
        }
        [Route("partner/discount-types")]
        [HttpGet]
        public IActionResult GetAllDiscountTypes()
        {
            var discountTypes = _partnerQueries.GetDiscountTypes();

            return discountTypes != null ? Ok(new { discountTypes }) : (IActionResult)NoContent();
        }
        [Route("partner/office-types")]
        [HttpGet]
        public IActionResult GetAllOfficeTypes()
        {
            var officeTypes = _partnerQueries.GetOfficeTypes();

            return officeTypes != null ? Ok(new { officeTypes }) : (IActionResult)NoContent();
        }
        [Route("partner/service-types")]
        [HttpGet]
        public IActionResult GetAllServiceTypes()
        {
            var serviceTypes = _partnerQueries.GetServiceTypes();

            return serviceTypes != null ? Ok(new { serviceTypes }) : (IActionResult)NoContent();
        }
        [Route("partner/sales-steps")]
        [HttpGet]
        public IActionResult GetAllSalesSteps()
        {
            var salesSteps = _partnerQueries.GetSalesSteps();

            return salesSteps != null ? Ok(new { salesSteps }) : (IActionResult)NoContent();
        }
        [Route("partner/country-types")]
        [HttpGet]
        public IActionResult GetAllCountryTypes()
        {
            var countryTypes = _partnerQueries.GetCountryTypes().OrderBy(x => x.Value);

            return countryTypes != null ? Ok(new { countryTypes }) : (IActionResult)NoContent();
        }
    }
}