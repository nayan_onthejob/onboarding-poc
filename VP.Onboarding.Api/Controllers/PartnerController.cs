﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using VP.Onboarding.Application.Commands;
using VP.Onboarding.Application.Queries.Contract;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/partner")]
    [ApiController]
    public class PartnerController : ControllerBase
    {

        private readonly IMediatorHelper _mediator;
        private readonly IPartnerQueries _partnerQueries;

        public PartnerController(IMediatorHelper mediator, IPartnerQueries partnerQueries)
        {
            _mediator = mediator;
            _partnerQueries = partnerQueries;
        }
        [Route("{partnerId:guid}/partner-view")]
        [HttpGet]
        public async Task<IActionResult> GetPartnerAsync(Guid partnerId)
        {
            var partner = await _partnerQueries.GetPartnerViewOfPartnerByIdAsync(partnerId);

            return partner != null ? Ok(partner) : (IActionResult)NotFound();
        }
        [Route("firm-key-details/{partnerId:guid}")]
        [HttpPut]
        public async Task<IActionResult> UpdateFirmKeyDetailsAsync(Guid partnerId, UpdateFirmKeyDetailsCommand updateFirmKeyDetailsCommand)
        {
            updateFirmKeyDetailsCommand.SetPartnerId(partnerId);

            var commandResult = await _mediator.Send(updateFirmKeyDetailsCommand);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("firm-address/{partnerId:guid}")]
        [HttpPut]
        public async Task<IActionResult> UpdateFirmAddressAsync(Guid partnerId, UpdateFirmAddressCommand updateFirmAddressCommand)
        {
            updateFirmAddressCommand.SetPartnerId(partnerId);

            var commandResult = await _mediator.Send(updateFirmAddressCommand);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("{partnerId:guid}/employee")]
        [HttpPost]
        public async Task<IActionResult> UpdateEmployeeInformationAsync(Guid partnerId, AddEmployeeCommand addEmployeeCommand)
        {
            addEmployeeCommand.SetPartnerId(partnerId);

            var commandResult = await _mediator.Send(addEmployeeCommand);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("{partnerId:guid}/employee/{employeeEmail}")]
        [HttpDelete]
        public async Task<IActionResult> UpdateEmployeeInformationAsync(Guid partnerId, string employeeEmail)
        {
            var deleteEmployeeCommand = new DeleteEmployeeCommand(partnerId, employeeEmail);

            var commandResult = await _mediator.Send(deleteEmployeeCommand);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("other-softwares-used/{partnerId:guid}")]
        [HttpPut]
        public async Task<IActionResult> UpdateOtherSoftwaresUserAsync(Guid partnerId, UpdateOtherSoftwaresUserCommand updateOtherSoftwaresUserCommand)
        {
            updateOtherSoftwaresUserCommand.SetPartnerId(partnerId);

            var commandResult = await _mediator.Send(updateOtherSoftwaresUserCommand);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }

        
    }
}