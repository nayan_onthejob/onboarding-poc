﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using VP.Onboarding.Application.Commands;
using VP.Onboarding.Application.Queries.Contract;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/partner")]
    [ApiController]
    public class PartnerForSalesController : ControllerBase
    {
        private readonly IMediatorHelper _mediator;
        private readonly IPartnerQueries _partnerQueries;

        public PartnerForSalesController(IMediatorHelper mediator, IPartnerQueries partnerQueries)
        {
            _mediator = mediator;
            _partnerQueries = partnerQueries;
        }
        [HttpPost]
        public async Task<IActionResult> PostAsync(CreatePartnerCommand generalInformationCommand)
        {
            var commandResult = await _mediator.Send(generalInformationCommand);
            return Ok(new { partnerId = commandResult });
        }
        [Route("{partnerId:guid}/sales-view")]
        [HttpGet]
        public async Task<IActionResult> GetPartnerAsync(Guid partnerId)
        {
            var partner = await _partnerQueries.GetSalesViewOfPartnerByIdAsync(partnerId);

            return partner != null ? Ok(partner) : (IActionResult)NotFound();
        }
        [Route("general-information/{partnerId:guid}")]
        [HttpPut]
        public async Task<IActionResult> UpdatePartnerGeneralInformationAsync(Guid partnerId, UpdateGeneralInformationCommand generalInformationCommand)
        {
            generalInformationCommand.SetPartnerId(partnerId);
            var commandResult = await _mediator.Send(generalInformationCommand);
            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("onboarding-information/{partnerId:guid}")]
        [HttpPut]
        public async Task<IActionResult> UpdatePartnerOnboardingInformationAsync(Guid partnerId, UpdateOnboardingInformationCommand onboardingInformationCommand)
        {
            onboardingInformationCommand.SetPartnerId(partnerId);
            var commandResult = await _mediator.Send(onboardingInformationCommand);
            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("other-options/{partnerId:guid}")]
        [HttpPut]
        public async Task<IActionResult> UpdatePartnerOtherOptionsAsync(Guid partnerId, UpdateOtherOptionsCommand otherOptionsCommand)
        {
            otherOptionsCommand.SetPartnerId(partnerId);
            var commandResult = await _mediator.Send(otherOptionsCommand);
            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        [Route("sales-review-and-submit")]
        [HttpPost]
        public async Task<IActionResult> ReviewAndSubmitAsync(SalesReviewAndSubmitCommand reviewAndSubmitCommand)
        {
            var commandResult = await _mediator.Send(reviewAndSubmitCommand);
            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
       
    }
}