﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using VP.CaseManagement.ExceptionHandling;
using VP.Onboarding.Common.ExceptionHandling;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Api.Filters
{
    public class OnboardingApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<OnboardingApiExceptionFilterAttribute> _logger;

        public OnboardingApiExceptionFilterAttribute(ILogger<OnboardingApiExceptionFilterAttribute> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            _logger.LogError(context.Exception, context.Exception.Message);

            if (context.Exception is DomainException domainException)
            {
                context.Result = new BadRequestObjectResult(new { Error = domainException.ErrorMessage });
            }
            else if (context.Exception is ResourceNotFoundException apiException)
            {
                context.Result = new NotFoundObjectResult(new { Error = apiException.Message });
            }
            else if (context.Exception is CaseManagementException caseManagementException)
            {
                context.Result = new BadRequestObjectResult(new { Error = caseManagementException.ErrorMessage });
            }
            else if (context.Exception is CaseManagementApiException caseManagementApiException)
            {
                context.Result = new StatusCodeResult((int)caseManagementApiException.StatusCode);
            }
            else
            {
                context.Result = new StatusCodeResult(500);
            }
        }
    }
}
