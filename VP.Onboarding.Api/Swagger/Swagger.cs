﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Linq;
using static VP.Onboarding.Api.Swagger.SwaggerConstants;

namespace VP.Onboarding.Api.Swagger
{
    public static class Swagger
    {
        public static void RegisterSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(
                options =>
                {
                    // resolve the IApiVersionDescriptionProvider service
                    // note: that we have to build a temporary service provider here because one has not been created yet
                    var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                    // add a swagger document for each discovered API version
                    // note: you might choose to skip or document deprecated API versions differently
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                    }

                    // add a custom operation filter which sets default values
                    options.OperationFilter<SwaggerDefaultValues>();

                    options.SchemaFilter<ReadOnlySchemaFilter>();

                    options.AddSecurityDefinition(SecurityDefinition.Bearer, new ApiKeyScheme
                    {
                        In = SecurityDefinition.ApiKeySchemeIn,
                        Name = SecurityDefinition.ApiKeySchemeName,
                        Type = SecurityDefinition.ApiKeySchemeType,
                    });

                    options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> { { SecurityDefinition.Bearer
                            , Enumerable.Empty<string>() } });
                });
        }
        private static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new Info()
            {
                Title = string.Format(SwaggerInfo.Title, description.ApiVersion),
                Version = description.ApiVersion.ToString(),
                Description = SwaggerInfo.Description,
            };

            if (description.IsDeprecated)
            {
                info.Description = string.Format(SwaggerInfo.DeprecatedDescription, description.ApiVersion.ToString());
            }

            return info;
        }

        public static void UseSwaggerVisualInterface(this IApplicationBuilder appBuilder, IApiVersionDescriptionProvider provider)
        {
            appBuilder.UseSwaggerUI(
                  options =>
                  {
                      // build a swagger endpoint for each discovered API version
                      foreach (var description in provider.ApiVersionDescriptions)
                      {
                          options.SwaggerEndpoint(string.Format(SwaggerInfo.EndpointUrl, description.GroupName), description.GroupName.ToUpperInvariant());
                      }
                  });
        }
    }
}
