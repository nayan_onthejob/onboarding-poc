﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
namespace VP.Onboarding.Api.Swagger
{
    /// <summary>
    /// Make private setters exposed for swagger by setting readonly as null
    /// </summary>
    public class ReadOnlySchemaFilter : ISchemaFilter
    {
        public void Apply(Schema schema, SchemaFilterContext context)
        {
            if (schema.Properties == null)
            {
                return;
            }

            var readOnlyProperties = schema.Properties.Where(x => x.Value.ReadOnly == true);
            foreach (var schemaProperty in readOnlyProperties)
            {
                schemaProperty.Value.ReadOnly = null;
            }
        }
    }
}
