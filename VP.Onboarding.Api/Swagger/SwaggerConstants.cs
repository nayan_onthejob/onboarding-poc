﻿namespace VP.Onboarding.Api.Swagger
{
    public static class SwaggerConstants
    {
        public static class SecurityDefinition
        {
            public const string Bearer = "Bearer";
            public const string ApiKeySchemeIn = "header";
            public const string ApiKeySchemeName = "Authorization";
            public const string ApiKeySchemeType = "apiKey";
        }

        public static class SwaggerInfo
        {
            public const string Title = "Onboarding API {0}";
            public const string Description = "Onboarding with Swagger, Swashbuckle, and API versioning.";
            public const string DeprecatedDescription = "Onboarding with Swagger, Swashbuckle, Version {0} has been deprecated.";
            public const string EndpointUrl = "/swagger/{0}/swagger.json";
            public const string EndpointName = "";

        }

    }
}
