﻿using System.Collections.Generic;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Shared
{
    public class Address : ValueObject
    {
        public Country Country { get; private set; }
        public string PostalCode { get; private set; }
        public string HouseNumber { get; private set; }
        public string StreetName { get; private set; }
        public string ExtraAddressLine { get; private set; }
        public string City { get; private set; }
        public Email FirmEmail { get; private set; }
        public PhoneNumber PhoneNumber { get; private set; }

        public Address(Country country, string postalCode, string houseNumber, string streetName, string extraAddressLine, string city, Email firmEmail, PhoneNumber phoneNumber)
        {
            Country = country;
            PostalCode = postalCode;
            HouseNumber = houseNumber;
            StreetName = streetName;
            ExtraAddressLine = extraAddressLine;
            City = city;
            FirmEmail = firmEmail;
            PhoneNumber = phoneNumber;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Country;
            yield return PostalCode;
            yield return HouseNumber;
            yield return StreetName;
            yield return ExtraAddressLine;
            yield return City;
            yield return FirmEmail;
            yield return PhoneNumber;
        }
    }
}
