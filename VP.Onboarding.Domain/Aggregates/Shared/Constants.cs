﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VP.Onboarding.Domain.Aggregates.Shared
{
    public static class Constant
    {
        public const string TYPE_OF_OFFICE_IS_MANDATORY = "Type of office is mandatory";
        public const string INFINE_LICENCE_NUMBER_IS_MANDATORY = "Infine licence number is mandatory";
        public const string HUBSPOT_ID_NUMBER_IS_MANDATORY = "HubspotId is mandatory";
        public const string ATLEAST_ONE_SERVICE_IS_MANDATORY = "Atleast one service is mandatory";
        public const string FIRST_CONTACT_DETAILS_ARE_MANDATORY = "First Contact details are mandatory";
        public const string DISCOUNT_UNTILL_SHOULD_NOT_BE_GREATER_THAN_CONTRACT_UNTILL = "Discount Until should not be greater than Contract Until";
        public const string CREDITS_UNTILL_DATE_IS_MANDATORY_WHEN_VALUE_IS_PROVIDED = "Credits Until date is mandatory when value is provided";
        public const string DISCOUNT_UNTILL_DATE_IS_MANDATORY = "Discount Until date is mandatory when value is provided";
        public const string INVALID_DISCOUNT = "Invalid Discount";
        public const string FIRST_CONTACT_NAME_IS_MANDATORY = "First Contact Name is mandatory";
        public const string FIRM_NAME_IS_MANDATORY = "Firm name is mandatory";
        public const string KVK_NUMBER_IS_MANDATORY = "KVK Number is mandatory";
        public const string COMPANY_NAME_IS_MANDATORY = "Company Name is mandatory";
        public const string STANDARD_COMPANY_CLASSIFICATION_IS_MANDATORY = "Standard Company Classification is mandatory";
        public const string INVALID_NUMBER_OF_EMPLOYEES = "Invalid Number Of Employees";
        public const string INVALID_NUMBER_NO_OF_ADMINISTRATIONS = "Invalid Number Of Administrations";
        public const string COUNTRY_IS_MANDATORY = "Country is mandatory";
        public const string POSTAL_CODE_IS_MANDATORY = "Postal code is mandatory";
        public const string STREET_NAME_IS_MANDATORY = "Street name is mandatory";
        public const string CITY_IS_MANDATORY = "City is mandatory";
        public const string EMAIL_IS_MANDATORY = "Email is mandatory";
        public const string PHONE_NUMBER_IS_MANDATORY = "Phone number is mandatory";
        public const string EMPLOYEE_ALREADY_EXISTS = "Employee already exists";
        public const string NO_SUCH_EMPLOYEE_EXISTS = "no such employee exists";
        public const string GENERAL_INFORMATION_IS_MANDATORY_TO_ADD_ONBOARDING_INFORMATION = "General information is mandatory to add onboarding information";
        public const string SALES_DETAILS_ARE_MANDATORY = "Sales details are mandatory";
        public const string FIRM_KEY_DETAILS_ARE_MANDATORY = "Firm key details are mandatory";
        public const string FIRM_ADDRESS_DETAILS_ARE_MANDATORY = "Firm Address details are mandatory";
        public const string EMPLOYEE_INFORMATION_IS_MANDATORY = "Employee information is mandatory";
        public const string ONBOARDING_INFORMATION_IS_MANDATORY_TO_ADD_OTHER_OPTIONS = "Onboarding information is mandatory to add Other Options";
        public const string CREDITS_UNTILL_SHOULD_NOT_BE_GREATER_THAN_CONTRACT_UNTILL = "Credits Until should not be greater than Contract Until";
        public const string GENERALINFORMATION_ONBOARDING_INFORMATION_AND_OTHER_OPTIONS_ARE_MANDATORY="General information, Onboarding information and Other options are mandatory";
        public const string ADMINISTRATION_SOFTWARE_USED_IS_MANDATORY = "Administration Software used is mandatory";
        public const string PAYROLL_SOFTWARE_USED_IS_MANDATORY="Payroll Software used is mandatory";
        public const string TAXATION_SOFTWARE_USED_IS_MANDATORY="Taxation Software used is mandatory";
        public const string CONTRACT_UNTILL_DATE_IS_MANDATORY_WHEN_MONTHLY_AMOUNT_IS_PROVIDED = "Contract Until date is mandatory when monthly amount is provided";
    }
}
