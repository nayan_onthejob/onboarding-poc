﻿using System.Collections.Generic;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Shared
{
    public class Country : Enumeration
    {
        public static readonly Country Italy = new Country(1, " Italy");
        public static readonly Country England = new Country(2, "England");
        public static readonly Country India = new Country(3, "India");
        public static readonly Country Netherlands = new Country(4, "Netherlands");
        public static readonly Country Spain = new Country(5, "Spain");


        public Country() { }
        protected Country(int value, string displayName) : base(value, displayName) { }
    }
}
