﻿using System.Collections.Generic;
using VP.Onboarding.Common.Utilities;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Shared
{
    public class Email : ValueObject
    {
        public Email(string email)
        {
            if (!string.IsNullOrEmpty(email) && !RegexUtilities.IsValidEmail(email))
                throw new DomainException("Invalid email format");
            Value = email;
        }
        public string Value { get; private set; }
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}
