﻿using System.Collections.Generic;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Shared
{
    public class BankAccountNumber : ValueObject
    {
        public string AccountNumber { get; private set; }

        public BankAccountNumber(string accountNumber)
        {       
            AccountNumber = accountNumber;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {   
            yield return AccountNumber;
        }
    }
}
