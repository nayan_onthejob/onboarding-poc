﻿using System.Collections.Generic;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Shared
{
    public class PhoneNumber:ValueObject
    {
        public string Value { get; set; }

        public PhoneNumber(string value)
        {
            Value = value;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}
