﻿using System.Collections.Generic;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Shared
{
    public class Amount : ValueObject
    {
        public Amount(decimal value)
        {
            Value = value;
        }
        public decimal Value { get; private set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }
    }
}
