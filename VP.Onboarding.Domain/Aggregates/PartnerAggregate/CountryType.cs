﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class CountryType: Enumeration
    {
        public static readonly CountryType TheNetherlands = new CountryType(1, "The Netherlands");
        public static readonly CountryType Others = new CountryType(2, "Others");

        public CountryType() { }
        public CountryType(int value, string displayName) : base(value, displayName) { }

    }
}
