﻿using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class TaxationSoftware : Enumeration
    {
        public TaxationSoftware() { }
        public TaxationSoftware(int value, string displayName) : base(value, displayName) { }

        public static readonly TaxationSoftware GeneralInformation = new TaxationSoftware(1, "General Information");
    }
}
