﻿using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class PayrollSoftware : Enumeration
    {
        public PayrollSoftware() { }
        public PayrollSoftware(int value, string displayName) : base(value, displayName) { }

        public static readonly PayrollSoftware Numbrs = new PayrollSoftware(1, "Numbrs");
        public static readonly PayrollSoftware Loket = new PayrollSoftware(2, "Loket");
    }
}
