﻿using System;
using System.Threading.Tasks;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts
{
    public interface IPartnerRepository : IRepository<Partner>
    {
        Task<bool> UpdateSalesChangesAsync(Partner partner);
        Task<bool> UpdatePartnerChangesAsync(Partner partner);
        Task<Partner> GetPartnerForSalesByPartnerIdAsync(Guid id);
        Task<Partner> GetPartnerByIdAsync(Guid id);
        Task<bool> SubmitSalesChangesAsync(Partner partner);
        Task<bool> CreatePartnerAsync(Partner partner);
    }
}
