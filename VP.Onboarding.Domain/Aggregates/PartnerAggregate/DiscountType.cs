﻿using System;
using System.Collections.Generic;
using System.Text;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class DiscountType : Enumeration
    {
        public static readonly DiscountType Percentage = new DiscountType(1, "%");
        public static readonly DiscountType Amount = new DiscountType(2, "€");
        
        public DiscountType() { }
        public DiscountType(int value, string displayName) : base(value, displayName) { }
    }
}
