﻿using System.Collections.Generic;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class FirstContact : ValueObject
    {
        public FirstContact(PersonName personName, Email email)
        {
            if (personName == null)
                throw new DomainException(Constant.FIRST_CONTACT_NAME_IS_MANDATORY);
            if (string.IsNullOrEmpty(email.Value))
                throw new DomainException(Constant.EMAIL_IS_MANDATORY);
            PersonName = personName;
            Email = email;
        }
        public PersonName PersonName { get; private set; }
        public Email Email { get; private set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return PersonName;
            yield return Email.Value;
        }
    }
}
