﻿using System.Linq;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class OnboardingStep : Enumeration
    {
        public static readonly OnboardingStep GeneralInformation = new OnboardingStep(1, "General Information");
        public static readonly OnboardingStep OnboardingInformation = new OnboardingStep(2, "Onboarding Information");
        public static readonly OnboardingStep OtherOptions = new OnboardingStep(3, "Other Options");
        public static readonly OnboardingStep SalesReviewAndSubmit = new OnboardingStep(4, "Review And Submit");

        public static readonly OnboardingStep FirmKeyDetails = new OnboardingStep(5, "Firm Key Details");
        public static readonly OnboardingStep FirmAddress = new OnboardingStep(6, "Firm Address");
        public static readonly OnboardingStep EmployeeInformation = new OnboardingStep(7, "Employee Information");
        public static readonly OnboardingStep OtherSoftwaresUsed = new OnboardingStep(8, "Other Softwares Used");
        public static readonly OnboardingStep UploadFirmLogo = new OnboardingStep(9, "Upload Firm Logo");
        public static readonly OnboardingStep InvoiceInformation = new OnboardingStep(10, "Invoice Information");
        public static readonly OnboardingStep PartnerReviewAndSubmit = new OnboardingStep(11, "Review and Submit");
        public OnboardingStep() { }
        public OnboardingStep(int value, string displayName) : base(value, displayName) { }
        

        public OnboardingStep GetNextOf(OnboardingStep salesStep)
        {
            return this.Id > salesStep.Id ? this : salesStep.GetNext();
        }
        public OnboardingStep GetNext()
        {
            var max = GetAll<OnboardingStep>().Max(x => x.Id);
            return this.Id + 1 > max ? this : FromValue<OnboardingStep>(this.Id + 1);
        }
    }
}
