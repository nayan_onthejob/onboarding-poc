﻿using System;
using System.Collections.Generic;
using System.Linq;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class Partner : Entity, IAggregateRoot
    {
        public string FirmName { get; private set; }
        public TypeOfOffice TypeOfOffice { get; private set; }
        public int HubspotId { get; private set; }
        private List<TypeOfService> _selectedServices = new List<TypeOfService>();
        public IReadOnlyList<TypeOfService> SelectedServices => _selectedServices;
        public string InfineLicenceNumber { get; private set; }
        public FirstContact FirstContact { get; private set; }
        public int DurationOfOnbarding { get; private set; }
        public MonthlyAmount MonthlyAmount { get; private set; }
        public Discount Discount { get; private set; }
        public Credits Credits { get; private set; }
        public string KvkNumber { get; private set; }
        public string CompanyName { get; private set; }
        public string StandardCompanyClassification { get; private set; }
        public int NoOfEmployees { get; private set; }
        public int NoOfAdministrations { get; private set; }
        public Address FirmAddress { get; private set; }
        private List<Employee> _employees = new List<Employee>();
        public IReadOnlyList<Employee> Employees => _employees;
        public AdministrationSoftware AdministrationSoftware { get; private set; }
        public PayrollSoftware PayrollSoftware { get; private set; }
        public TaxationSoftware TaxationSoftware { get; private set; }
        public Uri CompanyLogo { get; private set; }
        public BankAccountNumber InvoiceAccount { get; private set; }
        public Email InvoiceEmail { get; private set; }
        public OnboardingStep CurrentStep { get; private set; }

        public static Partner BuildExistingPartner(Guid Id, int CurrentStep)
        {
            return new Partner(Id)
            {
                CurrentStep = Enumeration.FromValue<OnboardingStep>(CurrentStep)
            };
        }

        private Partner(Guid id) { Id = id; }
        public Partner(string firmName)
        {
            Id = GetUniqueIdentifier();
            FirmName = !string.IsNullOrEmpty(firmName) ? firmName : throw new DomainException("Firm name is mandatory");
            CurrentStep = OnboardingStep.GeneralInformation;
        }

        public void UpdateGeneralInformation(string firmName, TypeOfOffice typeOfOffice, int hubspotId, string infineLicenceNumber, IEnumerable<TypeOfService> selectedTypeOfServices)
        {
            ValidateGeneralInformation(firmName, typeOfOffice, hubspotId, infineLicenceNumber, selectedTypeOfServices);
            FirmName = firmName;
            TypeOfOffice = typeOfOffice;
            HubspotId = hubspotId;
            InfineLicenceNumber = infineLicenceNumber;
            _selectedServices = selectedTypeOfServices.ToList();
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.GeneralInformation);
        }
        public void UpdateOnboardingInformation(FirstContact firstContact, int durationOfOnbarding, MonthlyAmount monthlyAmount)
        {
            if (CurrentStep.Id < OnboardingStep.OnboardingInformation.Id)
                throw new DomainException(Constant.GENERAL_INFORMATION_IS_MANDATORY_TO_ADD_ONBOARDING_INFORMATION);
            
            FirstContact = firstContact ?? throw new DomainException(Constant.FIRST_CONTACT_DETAILS_ARE_MANDATORY);
            DurationOfOnbarding = durationOfOnbarding;
            MonthlyAmount = monthlyAmount;
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.OnboardingInformation);
        }

        public void UpdateOtherOptions(Discount discount, Credits credits)
        {
            ValidateOtherOptions(discount, credits);
            Discount = discount;
            Credits = credits;
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.OtherOptions);
        }
        public void UpdateFirmKeyDetails(string kvkNumber, string companyName, string standardCompanyClassification, int noOfEmployees, int noOfAdministrations)
        {
            ValidateFirmKeyDetails(kvkNumber, companyName, standardCompanyClassification, noOfEmployees, noOfAdministrations);
            KvkNumber = kvkNumber;
            CompanyName = companyName;
            StandardCompanyClassification = standardCompanyClassification;
            NoOfEmployees = noOfEmployees;
            NoOfAdministrations = noOfAdministrations;
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.FirmKeyDetails);
        }
        public void UpdateFirmAddress(Address address)
        {
            if (CurrentStep.Id < OnboardingStep.FirmAddress.Id)
                throw new DomainException(Constant.FIRM_KEY_DETAILS_ARE_MANDATORY);

            if (address.Country == null)
                throw new DomainException(Constant.COUNTRY_IS_MANDATORY);
            if (string.IsNullOrEmpty(address.PostalCode))
                throw new DomainException(Constant.POSTAL_CODE_IS_MANDATORY);
            if (string.IsNullOrEmpty(address.StreetName))
                throw new DomainException(Constant.STREET_NAME_IS_MANDATORY);
            if (string.IsNullOrEmpty(address.City))
                throw new DomainException(Constant.CITY_IS_MANDATORY);
            if (string.IsNullOrEmpty(address.FirmEmail.Value))
                throw new DomainException(Constant.EMAIL_IS_MANDATORY);
            if (string.IsNullOrEmpty(address.PhoneNumber.Value))
                throw new DomainException(Constant.PHONE_NUMBER_IS_MANDATORY);

            FirmAddress = address;
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.FirmAddress);
        }
        public void AddEmployees(Employee employee)
        {
            if (CurrentStep.Id < OnboardingStep.EmployeeInformation.Id)
                throw new DomainException(Constant.FIRM_ADDRESS_DETAILS_ARE_MANDATORY);

            if (_employees.Any(x => x.Email.Value.Equals(employee.Email.Value, StringComparison.InvariantCultureIgnoreCase)))
                throw new DomainException(Constant.EMPLOYEE_ALREADY_EXISTS);
            //TODO:Need check if user existing in VPC 

            _employees.Add(employee);
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.EmployeeInformation);
        }
        public void DeleteEmployees(string employeeEmail)
        {
            var employee = _employees.SingleOrDefault(x => x.Email.Value.Equals(employeeEmail, StringComparison.InvariantCultureIgnoreCase));
            if (employee == default)
                throw new DomainException(Constant.NO_SUCH_EMPLOYEE_EXISTS);

            _employees.Remove(employee);
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.EmployeeInformation);
        }

        public void UpdateSoftwaresUsed(AdministrationSoftware administrationSoftware, PayrollSoftware payrollSoftware, TaxationSoftware taxationSoftware)
        {
            if (CurrentStep.Id < OnboardingStep.OtherSoftwaresUsed.Id)
                throw new DomainException(Constant.EMPLOYEE_INFORMATION_IS_MANDATORY);

            AdministrationSoftware = administrationSoftware ?? throw new DomainException(Constant.ADMINISTRATION_SOFTWARE_USED_IS_MANDATORY);
            PayrollSoftware = payrollSoftware ?? throw new DomainException(Constant.PAYROLL_SOFTWARE_USED_IS_MANDATORY);
            TaxationSoftware = taxationSoftware ?? throw new DomainException(Constant.TAXATION_SOFTWARE_USED_IS_MANDATORY);
            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.OtherSoftwaresUsed);
        }

        private void ValidateFirmKeyDetails(string kvkNumber, string companyName, string standardCompanyClassification, int noOfEmployees, int noOfAdministrations)
        {
            if (CurrentStep.Id < OnboardingStep.FirmKeyDetails.Id)
                throw new DomainException(Constant.SALES_DETAILS_ARE_MANDATORY);

            if (string.IsNullOrEmpty(kvkNumber))
                throw new DomainException(Constant.KVK_NUMBER_IS_MANDATORY);
            if (string.IsNullOrEmpty(companyName))
                throw new DomainException(Constant.COMPANY_NAME_IS_MANDATORY);
            if (string.IsNullOrEmpty(standardCompanyClassification))
                throw new DomainException(Constant.STANDARD_COMPANY_CLASSIFICATION_IS_MANDATORY);
            if (noOfEmployees == default)
                throw new DomainException(Constant.INVALID_NUMBER_OF_EMPLOYEES);
            if (noOfAdministrations == default)
                throw new DomainException(Constant.INVALID_NUMBER_NO_OF_ADMINISTRATIONS);
        }

        public void ReviewAndSubmitSalesChanges()
        {
            if (CurrentStep != OnboardingStep.SalesReviewAndSubmit)
                throw new DomainException(Constant.GENERALINFORMATION_ONBOARDING_INFORMATION_AND_OTHER_OPTIONS_ARE_MANDATORY);

            CurrentStep = CurrentStep.GetNextOf(OnboardingStep.SalesReviewAndSubmit);
        }

        private void ValidateOtherOptions(Discount discount, Credits credits)
        {
            if (CurrentStep.Id < OnboardingStep.OtherOptions.Id)
                throw new DomainException(Constant.ONBOARDING_INFORMATION_IS_MANDATORY_TO_ADD_OTHER_OPTIONS);

            if (MonthlyAmount != null && MonthlyAmount.IsContractUntilProvided() && discount.IsDiscountApplied() && discount.DiscountUntil > MonthlyAmount.ContractUntil)
                throw new DomainException(Constant.DISCOUNT_UNTILL_SHOULD_NOT_BE_GREATER_THAN_CONTRACT_UNTILL);

            if (MonthlyAmount != null && MonthlyAmount.IsContractUntilProvided() && credits.IsCreditsProvided() && credits.CreditsUntil > MonthlyAmount.ContractUntil)
                throw new DomainException(Constant.CREDITS_UNTILL_SHOULD_NOT_BE_GREATER_THAN_CONTRACT_UNTILL);
        }

        private static void ValidateGeneralInformation(string firmName, TypeOfOffice typeOfOffice, int hubspotId, string infineLicenceNumber, IEnumerable<TypeOfService> selectedTypeOfServices)
        {
            if (string.IsNullOrEmpty(firmName))
                throw new DomainException(Constant.FIRM_NAME_IS_MANDATORY);
            if (string.IsNullOrEmpty(infineLicenceNumber))
                throw new DomainException(Constant.INFINE_LICENCE_NUMBER_IS_MANDATORY);
            if (hubspotId == default)
                throw new DomainException(Constant.HUBSPOT_ID_NUMBER_IS_MANDATORY);
            if (typeOfOffice == null)
                throw new DomainException(Constant.TYPE_OF_OFFICE_IS_MANDATORY);
            if (!selectedTypeOfServices.Any())
                throw new DomainException("Atleast one service is mandatory");
        }
    }
}
