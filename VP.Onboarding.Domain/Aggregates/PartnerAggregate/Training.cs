﻿using System.Collections.Generic;
using System.Linq;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class Training : Enumeration
    {
        //TODO: Need to add actual trainings
        private const int BasicTainingsLimit = 4;
        public static readonly Training BasicTraining = new Training(1, "Basic Training");
        public static readonly Training CompileFinancialStatements = new Training(2, "Compile financial statements");
        public static readonly Training PartnerTraining = new Training(3, "Partner Training");

        public static readonly Training BasicTraining1 = new Training(4, "Basic Training");
        public static readonly Training CompileFinancialStatements1 = new Training(5, "Compile financial statements");
        public static readonly Training PartnerTraining1 = new Training(6, "Partner Training");
        public static readonly Training BasicTraining2 = new Training(7, "Basic Training");
        public static readonly Training CompileFinancialStatements2 = new Training(8, "Compile financial statements");
        public static readonly Training PartnerTraining2 = new Training(9, "Partner Training");
        public static readonly Training PartnerTraining3 = new Training(10, "Partner Training");
        
        public Training() { }
        public Training(int value, string displayName) : base(value, displayName) { }

        public static IReadOnlyList<Training> GetBasicTrainings()
        {
            return GetAll<Training>().Where(x => x.Id <= BasicTainingsLimit).ToList();
        }
        public static IReadOnlyList<Training> GetAdvanceTrainings()
        {
            return GetAll<Training>().Where(x => x.Id > BasicTainingsLimit).ToList();
        }
    }
}
