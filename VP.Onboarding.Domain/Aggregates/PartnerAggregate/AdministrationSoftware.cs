﻿using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class AdministrationSoftware : Enumeration
    {
        public AdministrationSoftware() { }
        public AdministrationSoftware(int value, string displayName) : base(value, displayName) { }

        public static readonly AdministrationSoftware AccountView = new AdministrationSoftware(1, "AccountView");
        public static readonly AdministrationSoftware AccountViewNet = new AdministrationSoftware(2, "AccountView.Net");
        public static readonly AdministrationSoftware AFASOnline = new AdministrationSoftware(3, "AFAS Online");
        public static readonly AdministrationSoftware AFASProfit = new AdministrationSoftware(4, "AFAS Profit");
        public static readonly AdministrationSoftware Auditfile = new AdministrationSoftware(5, "Auditfile (XAF)");
        public static readonly AdministrationSoftware CaseWare = new AdministrationSoftware(6, "CaseWare");
        public static readonly AdministrationSoftware CASHWeb = new AdministrationSoftware(7, "CASHWeb");
        public static readonly AdministrationSoftware DavilexAccount = new AdministrationSoftware(8, "Davilex Account");
        public static readonly AdministrationSoftware Boekhouden = new AdministrationSoftware(9, "e-Boekhouden");
        public static readonly AdministrationSoftware ExactGlobe = new AdministrationSoftware(10, "Exact Globe");
        public static readonly AdministrationSoftware ExactOnline = new AdministrationSoftware(11, "Exact Online");
        public static readonly AdministrationSoftware ExcelSjabloon = new AdministrationSoftware(12, "Excel sjabloon");
        public static readonly AdministrationSoftware ExcelUpload = new AdministrationSoftware(13, "Excel Upload");
        public static readonly AdministrationSoftware GroenVision = new AdministrationSoftware(14, "GroenVision");
        public static readonly AdministrationSoftware IMUIS = new AdministrationSoftware(15, "iMUIS");
        public static readonly AdministrationSoftware IMUISOnline = new AdministrationSoftware(16, "iMUIS Online");
        public static readonly AdministrationSoftware IMUISSQLServer = new AdministrationSoftware(17, "iMUIS SQL Server");
        public static readonly AdministrationSoftware King = new AdministrationSoftware(18, "King");
        public static readonly AdministrationSoftware MicrosoftDynamicsNAVClassic = new AdministrationSoftware(19, "Microsoft Dynamics NAV Classic (SQL Server)");
        public static readonly AdministrationSoftware Minox = new AdministrationSoftware(20, "Minox");
        public static readonly AdministrationSoftware OneBizz = new AdministrationSoftware(21, "OneBizz");
        public static readonly AdministrationSoftware Reeleezee = new AdministrationSoftware(22, "Reeleezee");
        public static readonly AdministrationSoftware SapBusinessOne = new AdministrationSoftware(23, "Sap Business One");
        public static readonly AdministrationSoftware SnelStartAccess = new AdministrationSoftware(24, "SnelStart (Access)");
        public static readonly AdministrationSoftware SnelStartOnlineDatabase = new AdministrationSoftware(25, "SnelStart (online Database)");
        public static readonly AdministrationSoftware SnelStartSQLServer = new AdministrationSoftware(26, "SnelStart (SQL Server)");
        public static readonly AdministrationSoftware SnelStartOnline = new AdministrationSoftware(27, "SnelStart Online");
        public static readonly AdministrationSoftware Twinfield = new AdministrationSoftware(28, "Twinfield");
        public static readonly AdministrationSoftware Unit4AuditionDatabase = new AdministrationSoftware(29, "Unit4 Audition database");
        public static readonly AdministrationSoftware Unit4MultiversOnlineORLokaal = new AdministrationSoftware(30, "Unit4 Multivers (Online/Lokaal)");
        public static readonly AdministrationSoftware Yuki = new AdministrationSoftware(31, "Yuki");
    }
}
