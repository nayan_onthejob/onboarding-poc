﻿using System;
using System.Collections.Generic;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class MonthlyAmount : ValueObject
    {
        public Amount Amount { get; private set; }
        public DateTime ContractUntil { get; private set; }

        public MonthlyAmount(Amount amount, DateTime contractUntil)
        {
            if (amount.Value != default && contractUntil == default)
                throw new DomainException(Constant.CONTRACT_UNTILL_DATE_IS_MANDATORY_WHEN_MONTHLY_AMOUNT_IS_PROVIDED);
            Amount = amount;
            ContractUntil = contractUntil;
        }
       
        public bool IsContractUntilProvided()
        {
            return ContractUntil != default;
        }
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Amount;
            yield return ContractUntil;
        }
    }
}
