﻿using System.Collections.Generic;
using System.Linq;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class Employee : Entity
    {
        public PersonName Name { get; private set; }
        public Email Email { get; private set; }
        public bool HasAdminRights { get; private set; }
        public IReadOnlyList<Training> RequiredTrainings { get; set; }

        public Employee(PersonName personName, Email email, bool hasAdminRights, IEnumerable<Training> trainings)
        {
            if (string.IsNullOrEmpty(email.Value))
                throw new DomainException(Constant.EMAIL_IS_MANDATORY);
            Id = GetUniqueIdentifier();
            Name = personName ?? throw new DomainException("Name is mandatory");
            Email = email;
            HasAdminRights = hasAdminRights;
            RequiredTrainings = trainings.ToList();
        }
    }
}
