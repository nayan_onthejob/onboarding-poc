﻿using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class TypeOfOffice : Enumeration
    {
        public static readonly TypeOfOffice Accountancy = new TypeOfOffice(1, "Accountancy");
        public static readonly TypeOfOffice Administration = new TypeOfOffice(2, "Administration");
        public static readonly TypeOfOffice SME = new TypeOfOffice(3, "SME");

        public TypeOfOffice() { }
        public TypeOfOffice(int value, string displayName) : base(value, displayName) { }
    }
}
