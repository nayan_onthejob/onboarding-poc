﻿using System;
using System.Collections.Generic;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class Credits : ValueObject
    {
        public Amount Amount { get; private set; }
        public DateTime CreditsUntil { get; private set; }

        public Credits(Amount amount, DateTime creditsUntil)
        {
            if (amount.Value != default &&  creditsUntil == default)
                throw new DomainException(Constant.CREDITS_UNTILL_DATE_IS_MANDATORY_WHEN_VALUE_IS_PROVIDED);
            Amount = amount;
            CreditsUntil = creditsUntil;
        }
        public bool IsCreditsProvided()
        {
            return Amount != default;
        }
        

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Amount;
            yield return CreditsUntil;
        }
    }
}
