﻿using System;
using System.Collections.Generic;
using VP.Onboarding.Domain.Aggregates.Shared;
using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class Discount : ValueObject
    {
        public decimal Value { get; private set; }
        public DiscountType DiscountType { get; set; }
        
        public DateTime DiscountUntil { get; set; }
        public Discount(decimal value, DiscountType discountType, DateTime discountUntil)
        {
            if (discountType == DiscountType.Percentage && value > 100)
                throw new DomainException(Constant.INVALID_DISCOUNT);
            if (value != default && discountUntil == default)
                throw new DomainException(Constant.DISCOUNT_UNTILL_DATE_IS_MANDATORY);

            Value = value;
            DiscountType = discountType;
            DiscountUntil = discountUntil;
        }

        public bool IsDiscountApplied()
        {
            return Value > 0;
        }
       
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
            yield return DiscountUntil;
        }
    }
}
