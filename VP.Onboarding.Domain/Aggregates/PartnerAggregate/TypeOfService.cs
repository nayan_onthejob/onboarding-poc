﻿using VP.Onboarding.Domain.SeedWork;

namespace VP.Onboarding.Domain.Aggregates.PartnerAggregate
{
    public class TypeOfService : Enumeration
    {
        public static readonly TypeOfService AdvisoryServices = new TypeOfService(1, "Advisory Service");
        public static readonly TypeOfService ComplilationService = new TypeOfService(2, "Complilation Service");
        

        public TypeOfService() { }
        public TypeOfService(int value, string displayName) : base(value, displayName) { }
    }
}
