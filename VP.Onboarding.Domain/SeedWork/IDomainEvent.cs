﻿using MediatR;
using System;

namespace VP.Onboarding.Domain.SeedWork
{
    public interface IDomainEvent : INotification
    {
        DateTime DateTimeEventOccurred { get; }
    }
}
