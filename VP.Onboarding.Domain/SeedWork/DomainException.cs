﻿using System;
namespace VP.Onboarding.Domain.SeedWork
{
    public class DomainException : Exception
    {
        public readonly string ErrorMessage;
        public DomainException(string errorMessage)
          : base(errorMessage)
        {
            ErrorMessage = errorMessage;
        }
        public DomainException(string errorMessage, Exception innerException)
            : base(errorMessage, innerException)
        {
            ErrorMessage = errorMessage;
        }
    }
}
