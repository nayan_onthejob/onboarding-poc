﻿using System;


namespace VP.Onboarding.Domain.SeedWork
{
    public static class Validators
    {
        public static string Emptyvalue(string strValue)
        {
            if (strValue == null) { throw new InvalidOperationException("Value Should not be Empty"); }
            else return strValue;
        }

        public static void ForLessEqualZero(int value, string parameterName)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForPrecedesDate(DateTime value, DateTime dateToPrecede, string parameterName)
        {
            if (value >= dateToPrecede)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForNullOrEmpty(string value, string parameterName)
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }
    }
}
