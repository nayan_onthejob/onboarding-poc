﻿using System.Reflection;

namespace VP.Onboarding.Domain
{
    public static class OnboardingDomain
    {
        public static Assembly Assembly => typeof(OnboardingDomain).Assembly;
    }
}
