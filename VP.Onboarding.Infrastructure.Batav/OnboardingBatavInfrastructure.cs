﻿using System.Reflection;

namespace VP.Onboarding.Infrastructure.Batav
{
    public static class OnboardingBatavInfrastructure
    {
        public static Assembly Assembly => typeof(OnboardingBatavInfrastructure).Assembly;
    }
}
