﻿using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using VP.CaseManagement;
using VP.CaseManagement.Models;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Infrastructure.Batav.CaseFileItems;
using VP.Onboarding.Infrastructure.Batav.Constants;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate.Contracts;

namespace VP.Onboarding.Infrastructure.Batav.Repository.Commands
{
    public class PartnerRepository : IPartnerRepository
    {
        private readonly ICaseManagementWrapper _caseManagementWrapper;
        public PartnerRepository(ICaseManagementWrapper caseManagementWrapper)
        {
            _caseManagementWrapper = caseManagementWrapper;
        }
        public async Task<Partner> GetPartnerForSalesByPartnerIdAsync(Guid id)
        {
            var caseInstanceId = new CaseInstanceId(id);

            var task = await _caseManagementWrapper.GetTaskByCaseIdAndTaskNameAsync(caseInstanceId, TaskName.SALES_ONBOARDING_A_PARTNER);
            var partnerCaseFile = task?.TaskRawOutput.ToObject<PartnerInfo>();

            return partnerCaseFile?.ToPartner();
        }
        public async Task<bool> CreatePartnerAsync(Partner partner)
        {
            var partnerInfoCaseInput = PartnerInfo.GetPartnerInfoCaseFileItem(partner);

            var result = await _caseManagementWrapper.CreateCaseAsync(new CaseInput
            {
                CaseInstanceId = new CaseInstanceId(partner.Id).ToString(),
                Name = CaseNames.PARTNER_ONBOARDING,
                Definition = $"{CaseNames.PARTNER_ONBOARDING}.xml",
                Inputs = JObject.FromObject(new { PartnerInfo = new { ResponsibleSalesPerson = "suzy@visionplanner.com" } }) //TODO: This will be replaced by responsible sales person in username
            });
            await _caseManagementWrapper.UpdateTaskAsync(new CaseInstanceId(partner.Id), TaskName.SALES_ONBOARDING_A_PARTNER, JObject.FromObject(partnerInfoCaseInput));
            return result.ToGuid() != Guid.Empty;
        }
        public async Task<bool> UpdateSalesChangesAsync(Partner partner)
        {
            var caseInstanceId = new CaseInstanceId(partner.Id);
            var partnerInfo = PartnerInfo.GetPartnerInfoCaseFileItem(partner);
            var result = await _caseManagementWrapper.UpdateTaskAsync(caseInstanceId, TaskName.SALES_ONBOARDING_A_PARTNER, JObject.FromObject(partnerInfo));
            return result;
        }
        public async Task<bool> SubmitSalesChangesAsync(Partner partner)
        {
            var caseInstanceId = new CaseInstanceId(partner.Id);
            var partnerInfo = PartnerInfo.GetPartnerInfoCaseFileItem(partner);
            var result = await _caseManagementWrapper.CompleteTaskAsync(caseInstanceId, TaskName.SALES_ONBOARDING_A_PARTNER,
                JObject.FromObject(new { PartnerInfo = partnerInfo }));

            return result;
        }

        public async Task<Partner> GetPartnerByIdAsync(Guid id)
        {
            var caseInstanceId = new CaseInstanceId(id);

            var task = await _caseManagementWrapper.GetTaskByCaseIdAndTaskNameAsync(caseInstanceId, TaskName.PARTNER_ONBOARDING);
            var partnerCaseFile = task?.TaskRawOutput.Count > 0 ? task?.TaskRawOutput.ToObject<PartnerInfo>() : task?.TaskInput[nameof(PartnerInfo)].ToObject<PartnerInfo>();

            return partnerCaseFile?.ToPartner();
        }

        public async Task<bool> UpdatePartnerChangesAsync(Partner partner)
        {
            var caseInstanceId = new CaseInstanceId(partner.Id);
            var partnerInfo = PartnerInfo.GetPartnerInfoCaseFileItem(partner);
            var result = await _caseManagementWrapper.UpdateTaskAsync(caseInstanceId, TaskName.PARTNER_ONBOARDING, JObject.FromObject(partnerInfo));
            return result;
        }
    }
}
