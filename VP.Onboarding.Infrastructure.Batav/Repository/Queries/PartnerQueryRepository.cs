﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VP.CaseManagement;
using VP.CaseManagement.Models;
using VP.Onboarding.Application.Queries.Dto;
using VP.Onboarding.Application.Queries.Repositories;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Infrastructure.Batav.CaseFileItems;
using VP.Onboarding.Infrastructure.Batav.Constants;

namespace VP.Onboarding.Infrastructure.Batav.Repository.Queries
{
    public class PartnerQueryRepository : IPartnerQueryRepository
    {
        private readonly ICaseManagementWrapper _caseManagementWrapper;
        public PartnerQueryRepository(ICaseManagementWrapper caseManagementWrapper)
        {
            _caseManagementWrapper = caseManagementWrapper;
        }

        public async Task<SalesViewOfPartnerDto> GetSalesViewOfPartnerByIdAsync(Guid id)
        {
            var partner = await GetPartnerFromTaskAsync(id, TaskName.SALES_ONBOARDING_A_PARTNER);

            return new SalesViewOfPartnerDto
            {
                PartnerId = partner.Id,
                CurrentStep = partner.CurrentStep.Id,
                GeneralInformation = PopulateGeneralInformation(partner),
                OnboardingInformation = PopulateOnboardingInformation(partner),
                OtherOptions = PopulateOtherOptions(partner)
            };
        }

        private async Task<Partner> GetPartnerFromTaskAsync(Guid partnerId, string taskName)
        {
            var caseInstanceId = new CaseInstanceId(partnerId);

            var task = await _caseManagementWrapper.GetTaskByCaseIdAndTaskNameAsync(caseInstanceId, taskName);

            var partnerCaseFile = task?.TaskRawOutput.Count > 0 ? task?.TaskRawOutput.ToObject<PartnerInfo>() : task?.TaskInput[nameof(PartnerInfo)].ToObject<PartnerInfo>();

            return partnerCaseFile?.ToPartner();
        }

        private static GeneralInformationDto PopulateGeneralInformation(Partner partner)
        {
            return new GeneralInformationDto(partner.FirmName
                , partner.TypeOfOffice.Id
                , partner.HubspotId
                , partner.SelectedServices?.Select(x => x.Id).ToList()
                , partner.InfineLicenceNumber);
        }

        private static OnboardingInformationDto PopulateOnboardingInformation(Partner partner)
        {
            return new OnboardingInformationDto(partner.FirstContact?.PersonName?.FirstName
                , partner.FirstContact?.PersonName?.MiddleName
                , partner.FirstContact?.PersonName?.LastName
                , partner.FirstContact?.Email.Value
                , partner.DurationOfOnbarding
                , partner.MonthlyAmount?.Amount.Value
                , partner.MonthlyAmount?.ContractUntil);
        }

        private static OtherOptionsDto PopulateOtherOptions(Partner partner)
        {
            return new OtherOptionsDto(partner.Discount?.DiscountType.Id
                , partner.Discount?.Value
                , partner.Discount?.DiscountUntil
                , partner.Credits?.Amount.Value
                , partner.Credits?.CreditsUntil);
        }
        

        public async Task<PartnerForPartnerDto> GetPartnerViewOfPartnerByIdAsync(Guid id)
        {
            var partner = await GetPartnerFromTaskAsync(id, TaskName.PARTNER_ONBOARDING);

            return new PartnerForPartnerDto
            {
                PartnerId = partner.Id,
                CurrentStep = partner.CurrentStep.Id,
                FirmKeyDetailsDto = PopulateFirmKeyDetails(partner),
                FirmAddressDto = PopulateFirmAddress(partner),
                EmployeeInformationDto = PopulateEmployeeInformation(partner),
                OtherSoftwaresUsedDto = PopulateOtherSoftwaresUsed(partner)
            };
        }

        private FirmKeyDetailsDto PopulateFirmKeyDetails(Partner partner)
        {
            return new FirmKeyDetailsDto(partner.KvkNumber
                , partner.CompanyName
                , partner.StandardCompanyClassification
                , partner.NoOfEmployees
                , partner.NoOfAdministrations);
        }
        private FirmAddressDto PopulateFirmAddress(Partner partner)
        {
            return partner.FirmAddress != default ? new FirmAddressDto(partner.FirmAddress.Country.Id
                , partner.FirmAddress.PostalCode
                , partner.FirmAddress.HouseNumber
                , partner.FirmAddress.StreetName
                , partner.FirmAddress.ExtraAddressLine
                , partner.FirmAddress.City
                , partner.FirmAddress.FirmEmail.Value
                , partner.FirmAddress.PhoneNumber.Value) : new FirmAddressDto();
        }
        private IReadOnlyList<EmployeeInformationDto> PopulateEmployeeInformation(Partner partner)
        {
            return partner.Employees != default && partner.Employees.Any() ? partner.Employees.Select(x => new EmployeeInformationDto(x.Email.Value
                , x.Name.FirstName
                , x.Name.MiddleName
                , x.Name.LastName
                , x.HasAdminRights
                , x.RequiredTrainings.Select(t => t.Id).ToList())).ToList() : default;
        }
        private OtherSoftwaresUsedDto PopulateOtherSoftwaresUsed(Partner partner)
        {
            return new OtherSoftwaresUsedDto(partner.AdministrationSoftware?.Id
                , partner.PayrollSoftware?.Id
                , partner.TaxationSoftware?.Id);
        }
    }
}
