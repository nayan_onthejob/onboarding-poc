﻿namespace VP.Onboarding.Infrastructure.Batav.Constants
{
    public static class TaskName
    {
        public const string SALES_ONBOARDING_A_PARTNER = "SalesOnboardingAPartner";
        public const string PARTNER_ONBOARDING = "PartnerOnboardingStep";
    }
}
