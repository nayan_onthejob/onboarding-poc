﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using VP.CaseManagement;

namespace VP.Onboarding.Infrastructure.Batav.ProcessTaskHandlers
{
    public class CreateUserHandler : IProcessTaskHandler
    {
        public string Name => nameof(CreateUserHandler);

        public Task<JObject> ProcessAsync(JObject data)
        {
            return Task.FromResult(data);
        }
    }
}
