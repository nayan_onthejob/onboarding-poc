﻿using Microsoft.AspNetCore.Builder;
using VP.Onboarding.Infrastructure.Batav;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static void AddBatavInfrastructure(this IServiceCollection services)
        {
            services.AddCaseManagement(x => x.UseBatav())
                .RegisterUserNameResolver<UserNameResolver>();

            services.Scan(scan => scan
                    .FromAssemblies(OnboardingBatavInfrastructure.Assembly)
                .AddClasses()
                .AsImplementedInterfaces()
                .WithTransientLifetime()
            );
        }
        public static void UseBatavInfrastructure(this IApplicationBuilder app)
        {
            app.UseCaseManagement();
        }
    }
}
