﻿using System;
using System.Collections.Generic;
using System.Linq;
using VP.Onboarding.Domain.Aggregates.PartnerAggregate;
using VP.Onboarding.Domain.SeedWork;
using VP.Onboarding.Domain.Shared;

namespace VP.Onboarding.Infrastructure.Batav.CaseFileItems
{
    public class PartnerInfo
    {
        private const string Delimiter = "|";

        public Guid Id { get; set; }
        public string FirmLogoUri { get; set; }
        public string ExtraAddress { get; set; }
        public int? AdministrationSoftwareId { get; set; }
        public string StandardCompanyClassification { get; set; }
        public string CompanyName { get; set; }
        public string KVKNumber { get; set; }
        public string HouseNumber { get; set; }
        public string City { get; set; }
        public bool IsApprovalRequired { get; set; }
        public int? PayrollSoftwareId { get; set; }
        public int NoOfEmployees { get; set; }
        public string StreetName { get; set; }
        public List<Employee> Employee { get; set; }
        public string InvoiceEmail { get; set; }
        public string ApproverName { get; set; }
        public int? TaxationSoftwareId { get; set; }
        public int? CountryId { get; set; }
        public string AccountNumber { get; set; }
        public string ApproverEmail { get; set; }
        public string PostalCode { get; set; }
        public string FirmPhoneNumber { get; set; }
        public int NoOfAdministrations { get; set; }
        public string FirmEmail { get; set; }
        public int CurrentStep { get; set; }
        public DateTime? ContractUntil { get; set; }
        public DateTime? CreditUntil { get; set; }
        public decimal? Credits { get; set; }
        public int DurationOfOnbarding { get; set; }
        public string FirmName { get; set; }
        public string FirstContactEmail { get; set; }
        public string TypeOfOffice { get; set; }
        public int HubspotId { get; set; }
        public string SelectedServices { get; set; }
        public string FirstContactPersonFirstName { get; set; }
        public string FirstContactPersonMiddleName { get; set; }
        public string FirstContactPersonLastName { get; set; }
        public string InfineLicenceNumber { get; set; }
        public decimal? MonthlyAmount { get; set; }
        public decimal? DiscountValue { get; set; }
        public int? DiscountType { get; set; }
        public DateTime? DiscountUntil { get; set; }
        public string ResponsibleSalesPerson { get; set; }


        public static PartnerInfo GetPartnerInfoCaseFileItem(Partner partner)
        {
            return partner != null ? new PartnerInfo
            {
                Id = partner.Id,
                CurrentStep = partner.CurrentStep.Id,
                ContractUntil = partner.MonthlyAmount?.ContractUntil,
                Credits = partner.Credits?.Amount.Value,
                DurationOfOnbarding = partner.DurationOfOnbarding,
                FirmName = partner.FirmName,
                FirstContactEmail = partner.FirstContact?.Email.Value,
                TypeOfOffice = partner.TypeOfOffice?.DisplayName,
                HubspotId = partner.HubspotId,
                SelectedServices = string.Join(Delimiter, partner.SelectedServices.Select(x => x.Id)),
                FirstContactPersonFirstName = partner.FirstContact?.PersonName?.FirstName,
                FirstContactPersonMiddleName = partner.FirstContact?.PersonName?.LastName,
                FirstContactPersonLastName = partner.FirstContact?.PersonName?.MiddleName,
                InfineLicenceNumber = partner.InfineLicenceNumber,
                MonthlyAmount = partner.MonthlyAmount?.Amount.Value,
                DiscountValue = partner.Discount?.Value,
                DiscountType = partner.Discount?.DiscountType?.Id,
                DiscountUntil = partner.Discount?.DiscountUntil,
                CreditUntil = partner.Credits?.CreditsUntil,

                KVKNumber = partner.KvkNumber,
                CompanyName = partner.CompanyName,
                StandardCompanyClassification = partner.StandardCompanyClassification,
                NoOfEmployees = partner.NoOfEmployees,
                NoOfAdministrations = partner.NoOfAdministrations,
                CountryId = partner.FirmAddress?.Country?.Id,
                PostalCode = partner.FirmAddress?.PostalCode,
                StreetName = partner.FirmAddress?.StreetName,
                ExtraAddress = partner.FirmAddress?.ExtraAddressLine,
                City = partner.FirmAddress?.City,
                FirmEmail = partner.FirmAddress?.FirmEmail.Value,
                FirmPhoneNumber = partner.FirmAddress?.PhoneNumber.Value,

                Employee = partner.Employees?.Select(x => new Employee
                {
                    EmailAddress = x.Email.Value,
                    FirstName = x.Name.FirstName,
                    MiddleName = x.Name.MiddleName,
                    LastName = x.Name.LastName,
                    HasAdminRights = x.HasAdminRights,
                    TrainingRequired = string.Join(Delimiter, x.RequiredTrainings.Select(y => y.Id))
                }).ToList(),

                AdministrationSoftwareId = partner.AdministrationSoftware?.Id,
                PayrollSoftwareId = partner.PayrollSoftware?.Id,
                TaxationSoftwareId = partner.TaxationSoftware?.Id

            } : null;
        }

        public Partner ToPartner()
        {
            var partner = Partner.BuildExistingPartner(Id, CurrentStep);
            if (CurrentStep > OnboardingStep.GeneralInformation.Id)
            {
                partner.UpdateGeneralInformation(FirmName
                                        , Enumeration.FromDisplayName<TypeOfOffice>(TypeOfOffice)
                                        , HubspotId
                                        , InfineLicenceNumber
                                        , SelectedServices.Split(Delimiter).Select(x => Enumeration.FromValue<TypeOfService>(int.Parse(x))));
            }
            if (CurrentStep > OnboardingStep.OnboardingInformation.Id)
            {
                partner.UpdateOnboardingInformation(new FirstContact(new PersonName(FirstContactPersonFirstName
                                                                        , FirstContactPersonMiddleName
                                                                        , FirstContactPersonLastName), new Email(FirstContactEmail))
                                                                        , DurationOfOnbarding
                                                                        , new MonthlyAmount(new Amount(MonthlyAmount.Value), ContractUntil.Value));
            }
            if (CurrentStep > OnboardingStep.OtherOptions.Id)
            {
                partner.UpdateOtherOptions(new Discount(DiscountValue.Value, Enumeration.FromValue<DiscountType>(DiscountType.Value), DiscountUntil.Value),
                                        new Credits(new Amount(Credits.Value), CreditUntil.Value));
            }
            if (CurrentStep > OnboardingStep.FirmKeyDetails.Id)
            {
                partner.UpdateFirmKeyDetails(KVKNumber, CompanyName, StandardCompanyClassification, NoOfEmployees, NoOfAdministrations);
            }
            if (CurrentStep > OnboardingStep.FirmAddress.Id)
            {
                partner.UpdateFirmAddress(new Address(
                    Enumeration.FromValue<Country>(CountryId.Value)
                    , PostalCode
                    , HouseNumber
                    , StreetName
                    , ExtraAddress
                    , City
                    , new Email(FirmEmail)
                    , new PhoneNumber(FirmPhoneNumber)));
            }
            if (CurrentStep > OnboardingStep.EmployeeInformation.Id && Employee.Any())
            {
                Employee.Select(x => new Domain.Aggregates.PartnerAggregate.Employee(
                  new PersonName(x.FirstName, x.MiddleName, x.LastName)
                  , new Email(x.EmailAddress)
                  , x.HasAdminRights
                  , x.TrainingRequired.Split(Delimiter).Select(t => Enumeration.FromValue<Training>(int.Parse(t))))).ToList().ForEach(x => partner.AddEmployees(x));
            }
            if (CurrentStep > OnboardingStep.OtherSoftwaresUsed.Id)
            {
                partner.UpdateSoftwaresUsed(Enumeration.FromValue<AdministrationSoftware>(AdministrationSoftwareId.Value)
                    , Enumeration.FromValue<PayrollSoftware>(PayrollSoftwareId.Value)
                    , Enumeration.FromValue<TaxationSoftware>(TaxationSoftwareId.Value));
            }
            return partner;
        }
    }
}
