﻿namespace VP.Onboarding.Infrastructure.Batav.CaseFileItems
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string TrainingRequired { get; set; }
        public bool HasAdminRights { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
    }
}
