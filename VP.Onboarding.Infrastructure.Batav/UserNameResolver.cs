﻿using System.Threading.Tasks;
using VP.CaseManagement;

namespace VP.Onboarding.Infrastructure.Batav
{
    public class UserNameResolver : IUserNameResolver
    {
        public Task<string> ResolverAsync()
        {
            return Task.FromResult("suzy@visionplanner.com"); //TODO: This will be replaced by logged in username
        }
    }
}
