﻿using MediatR;

namespace VP.Onboarding.Common.Mediator.Abstraction
{
    public interface ICommandHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> 
        where TRequest : ICommand<TResponse>
    {
    }
}
