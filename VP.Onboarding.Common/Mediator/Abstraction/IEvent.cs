﻿using MediatR;
using System;

namespace VP.Onboarding.Common.Mediator.Abstraction
{
    public interface IEvent : INotification
    {
        //DateTime OcuuredAt { get; set; }
    }
}
