﻿namespace VP.Onboarding.Common.Mediator.Abstraction
{
    public class EventResponse
    {
        public bool IsSuccess { get; set; }
    }
}
