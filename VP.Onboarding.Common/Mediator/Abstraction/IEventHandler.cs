﻿using MediatR;

namespace VP.Onboarding.Common.Mediator.Abstraction
{
    public interface IEventHandler<T> : INotificationHandler<T> where T : IEvent
    {
    }
}
