﻿using MediatR;

namespace VP.Onboarding.Common.Mediator.Abstraction
{
    public interface ICommand<out T> : IRequest<T>
    {
    }
}
