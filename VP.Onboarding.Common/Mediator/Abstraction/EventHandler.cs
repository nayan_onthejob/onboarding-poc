﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace VP.Onboarding.Common.Mediator.Abstraction
{

    public abstract class EventHandler<TEvent> : INotificationHandler<TEvent> where TEvent : IEvent
    {
        public abstract Task HandleEvent(TEvent @event, CancellationToken cancellationToken);

        public async Task Handle(TEvent @event, CancellationToken cancellationToken)
        {
            await HandleEvent(@event, cancellationToken);
        }
    }
}
