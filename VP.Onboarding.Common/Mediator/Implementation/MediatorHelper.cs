﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VP.Onboarding.Common.Mediator.Abstraction;

namespace VP.Onboarding.Common.Mediator.Implementation
{
    public class MediatorHelper : IMediatorHelper
    {
        private readonly IMediator _mediator;
        public MediatorHelper(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<TResponse> Send<TResponse>(ICommand<TResponse> command, CancellationToken cancellationToken = default)
        {
            return await _mediator.Send(command, cancellationToken);
        }

        public async Task Publish(object notification, CancellationToken cancellationToken = default)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                await Task.FromCanceled(cancellationToken);
                return;
            }
            await _mediator.Publish(notification, cancellationToken);
        }

        public async Task Publish<TEvent>(IEvent notification, CancellationToken cancellationToken = default)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                await Task.FromCanceled(cancellationToken);
                return;
            }
            await _mediator.Publish(notification, cancellationToken);
        }

        public async Task<EventResponse> AwaitablePublishAsync(IEvent notification, CancellationToken cancellationToken = default)
        {
            var response = new EventResponse();
            if (cancellationToken.IsCancellationRequested)
            {
                return await Task.FromResult(response);
            }
            var notificationResponse = _mediator.Publish(notification, cancellationToken);
            await notificationResponse;
            if (notificationResponse.IsCompletedSuccessfully)
            {
                response.IsSuccess = true;
            }
            return await Task.FromResult(response);

        }

    }
}
