﻿using System.Reflection;

namespace VP.Onboarding.Common
{
    public static class OnboardingCommon
    {
        public static Assembly Assembly => typeof(OnboardingCommon).Assembly;
    }
}
