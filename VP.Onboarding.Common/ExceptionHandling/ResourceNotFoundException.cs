﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace VP.Onboarding.Common.ExceptionHandling
{
    public class ResourceNotFoundException : OnboardingApiException
    {
        public ResourceNotFoundException(string message)
            : base(message)
        {

        }
    }
}
