﻿using System;
using System.Net.Http;

namespace VP.Onboarding.Common.ExceptionHandling
{
    public class OnboardingApiException : Exception
    {
        public OnboardingApiException(string message)
            : base(message)
        {
            
        }
    }
}
