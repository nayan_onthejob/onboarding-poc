﻿using System.Reflection;

namespace VP.Onboarding.Infrastructure
{
    public static class OnboardingInfrastructure
    {
        public static Assembly Assembly => typeof(OnboardingInfrastructure).Assembly;
    }
}
